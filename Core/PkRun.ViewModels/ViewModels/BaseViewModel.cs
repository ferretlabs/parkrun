﻿using System.Threading.Tasks;
using Cimbalino.Toolkit.Handlers;
using Cimbalino.Toolkit.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using ParkRunPortable.Logging;
using PkRun.Core.Services;

namespace PkRun.ViewModels
{
    public abstract class BaseViewModel : ViewModelBase
    {
        protected readonly ILogger Logger;
        protected readonly IEnvironmentService EnvironmentService;

        private readonly IAnalyticsService _analytics;

        protected BaseViewModel()
        {
            _analytics = SimpleIoc.Default.GetInstance<IAnalyticsService>();
            Logger = SimpleIoc.Default.GetInstance<ILogger>();
            EnvironmentService = SimpleIoc.Default.GetInstance<IEnvironmentService>();
        }

        public bool ProgressIsVisible { get; set; }
        public string ProgressText { get; set; }

        protected virtual void UpdateProperties() { }
        
        protected void Track(string message)
        {
            _analytics.Track(message);
        }

        protected void SetProgressBar(string text)
        {
            ProgressIsVisible = true;
            ProgressText = text;

            UpdateProperties();
        }

        protected void SetProgressBar()
        {
            ProgressIsVisible = false;
            ProgressText = string.Empty;

            UpdateProperties();
        }
    }

    public abstract class PageBaseViewModel : BaseViewModel, IHandleNavigatedTo, IHandleNavigatedFrom
    {
        public virtual string PageTitle { get; set; }

        public virtual Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            return Task.FromResult(0);
        }

        public virtual Task OnNavigatedFromAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            return Task.FromResult(0);
        }
    }
}
