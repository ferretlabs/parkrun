﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using GalaSoft.MvvmLight.Command;
using ParkRunPortable;
using PkRun.Command;
using PkRun.Core.Services;
using PkRun.ViewModels.Items;
using INavigationService = PkRun.Core.Services.INavigationService;

namespace PkRun.ViewModels
{
    public class NavBarViewModel : BaseViewModel
    {
        private readonly INavigationService _navigationService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IParkRunClient _parkRunClient;
        private readonly IDispatcherService _dispatcherService;

        public NavBarViewModel(
            INavigationService navigationService,
            IAuthenticationService authenticationService,
            IParkRunClient parkRunClient,
            IDispatcherService dispatcherService)
        {
            _navigationService = navigationService;
            _authenticationService = authenticationService;
            _parkRunClient = parkRunClient;
            _dispatcherService = dispatcherService;

            NavigateToCommand = new AsyncRelayCommand<NavMenuItem>(NavigateTo);
            SignOutCommand = new RelayCommand(_authenticationService.SignOut);
            NavigateToProfileCommand = new AsyncRelayCommand(() => Navigate(x => x.NavigateToAthlete(_authenticationService.SignedInAthlete)));
            NavigateToSettingsCommand = new AsyncRelayCommand(() => Navigate(x => x.NavigateToSettings()));

            SetMenuItems();

            Athlete = new AthleteItemViewModel(_authenticationService.SignedInAthlete, _parkRunClient, _authenticationService);

            _authenticationService.AuthenticationChanged += OnAuthenticationChanged;
        }

        public ObservableCollection<NavMenuItem> MenuItems { get; } = new ObservableCollection<NavMenuItem>();

        public AsyncRelayCommand<NavMenuItem> NavigateToCommand { get; }
        public AsyncRelayCommand NavigateToProfileCommand { get; }
        public AsyncRelayCommand NavigateToSettingsCommand { get; }

        public RelayCommand SignOutCommand { get; }

        private void SetMenuItems()
        {
            MenuItems.Add(new NavMenuItem(x => x.NavigateToHome(), "Home", "icon.png"));
            MenuItems.Add(new NavMenuItem(x => x.NavigateToEvent(new EventNavigationArgs(Athlete.HomeEventId)), "Home Run", "event.png"));
        }

        private void OnAuthenticationChanged(object sender, AuthenticationChangedArgs args)
        {
            Athlete = args.IsSignedIn ? new AthleteItemViewModel(_authenticationService.SignedInAthlete, _parkRunClient, _authenticationService) : null;
        }

        public AthleteItemViewModel Athlete { get; set; }

        private Task NavigateTo(NavMenuItem item)
        {
            return Navigate(item.NavigationFunction);
        }

        private Task Navigate(Func<INavigationService, Task> func)
        {
            return func.Invoke(_navigationService).ContinueWith(_ => _dispatcherService.InvokeOnUiThreadAsync(() => _navigationService.ClearBackstack()));
        }
    }

    public class NavMenuItem
    {
        public NavMenuItem(Func<INavigationService, Task> navigationFunction, string title, string icon)
        {
            NavigationFunction = navigationFunction;
            Title = title;
            Icon = icon;
        }

        public Func<INavigationService, Task> NavigationFunction { get; }
        public string Title { get; }
        public string Icon { get; }
    }
}