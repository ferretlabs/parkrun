﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using GalaSoft.MvvmLight.Command;
using ParkRunPortable;
using ParkRunPortable.Model;
using ParkRunPortable.Model.Requests;
using ParkRunPortable.Model.Responses;
using PkRun.Core.Extensions;
using PkRun.Core.Services;
using INavigationService = PkRun.Core.Services.INavigationService;

namespace PkRun.ViewModels
{
    public class EventViewModel : PageBaseViewModel
    {
        private readonly IParkRunClient _parkRunClient;
        private readonly INavigationService _navigationService;
        private readonly ICancellationManager _cancellationManager;
        private int _eventId;

        private static readonly Regex FrameSrcRegex = new Regex("<iframe.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase);
        
        public EventViewModel(
            IParkRunClient parkRunClient,
            INavigationService navigationService,
            ICancellationManager cancellationManager)
        {
            _parkRunClient = parkRunClient;
            _navigationService = navigationService;
            _cancellationManager = cancellationManager;

            NavigateToLatestRunResultsCommand = new RelayCommand(NavigateToLatestRunResults);
        }

        public Event Event { get; set; }
        public CourseDataResponse CourseDataResponse { get; set; }
        public Run LatestRun { get; set; }
        public string LatestRunDate => LatestRun?.EventDate.ToString("D");
        public bool DisplayLatestRunResults => !string.IsNullOrEmpty(LatestRunDate);

        public List<CourseDataInfo> EventDetails => CourseDataResponse?.Items.Where(x => !string.IsNullOrEmpty(x.Value)).ToList();

        public string Name => Event?.EventLongName;

        public string StartTime => GetDetailsValue(CourseDataResponse, CourseDataMetaKey.StartTime);

        public RelayCommand NavigateToLatestRunResultsCommand { get; }

        public string MapUrl { get; set; } = string.Empty;

        public override Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            var args = eventArgs.Parameter as EventNavigationArgs;

            if (eventArgs.NavigationMode != NavigationServiceNavigationMode.Back
                || args?.EventId != Event?.EventNumber)
            {
                if (args == null)
                {
                    _navigationService.GoBack();
                    return Task.FromResult(0);
                }

                Event = null;
                MapUrl = string.Empty;
                CourseDataResponse = null;
                LatestRun = null;

                _eventId = args.EventId;

                LoadDetails().DontAwait();
            }

            return base.OnNavigatedToAsync(eventArgs);
        }

        private async Task LoadDetails()
        {
            try
            {
                SetProgressBar("Loading event details...");

                var token = _cancellationManager.CurrentCancellationToken;

                await GetMapUrl(token);

                await GetEventDetails(token);

                await GetLatestRun(token);

                await GetCourseData(token);
            }
            catch (ParkRunException)
            {
            }
            finally
            {
                SetProgressBar();
            }
        }

        private async Task GetCourseData(CancellationToken token)
        {
            try
            {
                CourseDataResponse = await _parkRunClient.GetCourseDataAsync(_eventId, token);
            }
            catch (ParkRunException)
            {
            }
        }

        private async Task GetLatestRun(CancellationToken token)
        {
            try
            {
                var latestRuns = await _parkRunClient.GetRunsForEventAsync(_eventId, new FilterOptions {Limit = 2, SortOrder = SortOrder.Descending, OrderFields = new List<string> {"EventDate"}}, cancellationToken: token);

                var latestRunWithResults = latestRuns?.Items?.FirstOrDefault(x => x.NumberRunners > 0);
                LatestRun = latestRunWithResults;
            }
            catch (ParkRunException)
            {
            }
        }

        private async Task GetEventDetails(CancellationToken token)
        {
            try
            {
                var details = await _parkRunClient.GetEventAsync(_eventId, token);
                Event = details;
            }
            catch (ParkRunException)
            {
            }
        }

        private async Task GetMapUrl(CancellationToken token)
        {
            try
            {
                var courseRoute = await _parkRunClient.GetCourseRouteMapAsync(_eventId, token);
                var mapUrl = SanitiseCourseRoute(courseRoute);
                MapUrl = mapUrl;
            }
            catch (ParkRunException)
            {
            }
        }

        private static string SanitiseCourseRoute(string courseRoute)
        {
            if (string.IsNullOrEmpty(courseRoute))
            {
                return string.Empty;
            }

            var mapUrlMatches = FrameSrcRegex.Match(courseRoute);
            var mapUrl = WebUtility.HtmlDecode(mapUrlMatches.Groups[1].Value);

            if (string.IsNullOrEmpty(mapUrl))
            {
                return string.Empty;
            }

            if (!Uri.TryCreate(mapUrl, UriKind.Absolute, out Uri url))
            {
                return string.Empty;
            }

            var uri = new UriBuilder("https", url.Host)
            {
                Path = url.AbsolutePath
            };

            if (!string.IsNullOrEmpty(url.Query))
            {
                uri.Query = url.Query.Substring(1);
            }

            return url.ToHttps();
        }

        private static string GetDetailsValue(CourseDataResponse courseDataResponse, CourseDataMetaKey key)
        {
            return courseDataResponse?.Items?.FirstOrDefault(x => x.Metakey == key)?.Value;
        }

        private void NavigateToLatestRunResults()
        {
            _navigationService.NavigateToRunResults(new RunResultsNavigationArgs(Event, LatestRun));
        }
    }

    public class EventNavigationArgs
    {
        public int EventId { get; }

        public EventNavigationArgs(int eventId)
        {
            EventId = eventId;
        }
    }
}
