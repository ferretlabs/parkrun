﻿using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using ParkRunPortable;
using ParkRunPortable.Model;
using PkRun.Core.Extensions;
using PkRun.Core.Services;
using PkRun.ViewModels.Items;
using INavigationService = PkRun.Core.Services.INavigationService;

namespace PkRun.ViewModels
{
    public class AthleteViewModel : PageBaseViewModel
    {
        private readonly IParkRunClient _parkRunClient;
        private readonly INavigationService _navigationService;
        private readonly ICancellationManager _cancellationManager;
        private readonly IAuthenticationService _authenticationService;

        public AthleteViewModel(
            IParkRunClient parkRunClient, 
            INavigationService navigationService,
            ICancellationManager cancellationManager,
            IAuthenticationService authenticationService)
        {
            _parkRunClient = parkRunClient;
            _navigationService = navigationService;
            _cancellationManager = cancellationManager;
            _authenticationService = authenticationService;
        }

        public AthleteItemViewModel Athlete { get; set; }

        public string NameAndAge => $"{Athlete?.FullName} {Athlete?.AgeFormatted}";
        public override string PageTitle => Athlete?.FullName;

        public string NumberOfRuns { get; set; }

        public override Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            Athlete = null;

            var athlete = eventArgs.Parameter as Athlete;
            if (athlete != null)
            {
                Athlete = new AthleteItemViewModel(athlete, _parkRunClient, _authenticationService);
                UpdateAtheleteInfo(athlete.AthleteId).DontAwait();
            }
            else if (eventArgs.Parameter is int)
            {
                var athleteId = (int) eventArgs.Parameter;
                UpdateAtheleteInfo(athleteId).DontAwait();
            }
            else
            {
                _navigationService.GoBack();
            }

            return base.OnNavigatedToAsync(eventArgs);
        }

        private async Task UpdateAtheleteInfo(int athleteId)
        {
            try
            {
                var token = _cancellationManager.CurrentCancellationToken;
                var athlete = await _parkRunClient.GetAthleteProfileAsync(athleteId, true, token);
                
                if (Athlete == null)
                {
                    Athlete = new AthleteItemViewModel(athlete, _parkRunClient, _authenticationService);
                }
                else
                {
                    Athlete.UpdateAthlete(athlete);
                }
            }
            catch (ParkRunException)
            {
                
            }
        }
    }
}
