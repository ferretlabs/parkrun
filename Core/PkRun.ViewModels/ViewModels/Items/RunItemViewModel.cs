﻿using ParkRunPortable.Model;

namespace PkRun.ViewModels.Items
{
    public class RunItemViewModel : BaseViewModel
    {
        public Run Run { get; }
        public RunItemViewModel(Run run)
        {
            Run = run;
        }
    }
}