﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Humanizer;
using ParkRunPortable;
using ParkRunPortable.Model;
using PkRun.Command;
using PkRun.Core.Services;
using PkRun.Core.Extensions;

namespace PkRun.ViewModels.Items
{
    public class AthleteItemViewModel : BaseViewModel
    {
        private readonly IParkRunClient _parkRunClient;
        private readonly IAuthenticationService _authenticationService;

        public AthleteItemViewModel(
            Athlete athlete, 
            IParkRunClient parkRunClient, 
            IAuthenticationService authenticationService)
        {
            Athlete = athlete;
            _parkRunClient = parkRunClient;
            _authenticationService = authenticationService;
        }

        public Athlete Athlete { get; set; }

        public string FirstName => Athlete.FirstName?.Transform(To.LowerCase, To.TitleCase);
        public string LastName => Athlete.LastName?.Transform(To.LowerCase, To.TitleCase);
        public string FullName => $"{FirstName} {LastName}";

        public string DateOfBirth => Athlete.DateOfBirth?.ToString(CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern);
        public string Avatar => !Athlete.Avatar?.StartsWith("http://") ?? false ? $"https://{Athlete.Avatar}" : new Uri(Athlete.Avatar).ToHttps();
        public int HomeEventId => Athlete.HomeRunId;
        public string HomeRunName => Athlete.HomeRunName;
        public string ClubName => Athlete.ClubName;
        public string Age => Athlete?.DateOfBirth.Age(EnvironmentService);
        public string AgeFormatted => string.IsNullOrEmpty(Age) ? null : $"({Age})";

        public bool IsSignedInUser => Athlete.AthleteId == _authenticationService.SignedInAthlete.AthleteId;

        public AsyncRelayCommand FollowUserCommand => new AsyncRelayCommand(FollowUser);

        public void UpdateAthlete(Athlete athlete)
        {
            Athlete = athlete;
        }

        private async Task FollowUser()
        {
            try
            {
                await _parkRunClient.SendFollowRequestAsync(Athlete.AthleteId);
            }
            catch (ParkRunException)
            {
                
            }
        }
    }
}
