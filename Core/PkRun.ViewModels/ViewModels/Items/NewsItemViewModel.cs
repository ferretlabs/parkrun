﻿using System.Threading.Tasks;
using ParkRunPortable.Model;
using PkRun.Command;
using PkRun.Core.Services;

namespace PkRun.ViewModels.Items
{
    public class NewsItemViewModel : BaseViewModel
    {
        private readonly INewsApiHandler _newsApiHandler;
        private readonly INavigationService _navigationService;
        private readonly ICancellationManager _cancellationManager;
        public NewsItem NewsItem { get; private set; }

        public NewsItemViewModel(
            NewsItem newsItem, 
            INewsApiHandler newsApiHandler, 
            INavigationService navigationService,
            ICancellationManager cancellationManager)
        {
            _newsApiHandler = newsApiHandler;
            _navigationService = navigationService;
            _cancellationManager = cancellationManager;
            NewsItem = newsItem;

            NavigateToItemCommand = new AsyncRelayCommand(NavigateToItem);
            LoadPostDetailsCommand = new AsyncRelayCommand(LoadPostDetails);
        }

        private Task NavigateToItem() => _navigationService.NavigateToNewsItem(NewsItem);

        public AsyncRelayCommand NavigateToItemCommand { get; }
        public AsyncRelayCommand LoadPostDetailsCommand { get; }
        
        private async Task LoadPostDetails()
        {
            var token = _cancellationManager.CurrentCancellationToken;
            var item = await _newsApiHandler.GetNewsItem(NewsItem.EventNumber, NewsItem.Id, token);

            NewsItem = item;
        }
    }
}
