﻿using System.Threading.Tasks;
using Humanizer;
using ParkRunPortable.Model;
using PkRun.Command;
using PkRun.Core.Extensions;
using PkRun.Core.Services;

namespace PkRun.ViewModels.Items
{
    public class ResultItemViewModel : BaseViewModel
    {
        private readonly INavigationService _navigationService;
        private readonly IAuthenticationService _authenticationService;
        public Result Result { get; }

        public ResultItemViewModel(
            Result result, 
            INavigationService navigationService,
            IAuthenticationService authenticationService)
        {
            _navigationService = navigationService;
            _authenticationService = authenticationService;
            Result = result;

            NavigateToUserCommand = new AsyncRelayCommand(NavigateToUser);
        }

        public string FinishPosition => Result.FinishPosition.ToString();
        public string RunnerName => $"{Result.FirstName} {Result.LastName}".Transform(To.LowerCase, To.TitleCase);
        public string FinishTime => Result.RunTime.ToRunTime();
        public bool IsPersonalBest => Result.PersonalBest;

        public bool IsSignedInUser => Result.AthleteId == _authenticationService.SignedInAthlete.AthleteId;

        public AsyncRelayCommand NavigateToUserCommand { get; } 

        private Task NavigateToUser()
        {
            return _navigationService.NavigateToAthlete(Result.AthleteId);
        }
    }
}
