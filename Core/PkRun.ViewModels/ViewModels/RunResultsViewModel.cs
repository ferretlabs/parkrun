﻿using System.Globalization;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using ParkRunPortable;
using ParkRunPortable.Model;
using PkRun.Collections;
using PkRun.Core.Services;
using INavigationService = PkRun.Core.Services.INavigationService;

namespace PkRun.ViewModels
{
    public class RunResultsViewModel : PageBaseViewModel
    {
        private readonly IParkRunClient _parkRunClient;
        private readonly INavigationService _navigationService;

        public RunResultsViewModel(
            IParkRunClient parkRunClient,
            INavigationService navigationService,
            IAuthenticationService authenticationService)
        {
            _parkRunClient = parkRunClient;
            _navigationService = navigationService;

            Results = new ResultsCollection(new ResultsSource(_parkRunClient, _navigationService, authenticationService));
        }

        public ResultsCollection Results { get; set; }
        public Run EventRun { get; set; }
        public Event Event { get; set; }

        public override string PageTitle => $"{Event?.EventLongName} - #{EventRun?.RunId} - {EventRun?.EventDate.ToString(CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern)}";

        public override Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            var args = eventArgs.Parameter as RunResultsNavigationArgs;
            if (args != null)
            {
                SetRun(args.Run);
                Event = args.Event;
            }
            else
            {
                _navigationService.GoBack();
            }

            return base.OnNavigatedToAsync(eventArgs);
        }

        private void SetRun(Run run)
        {
            EventRun = run;
            Results.SetRunInformation(EventRun);
        }
    }

    public class RunResultsNavigationArgs
    {
        public Event Event { get; }
        public Run Run { get; }

        public RunResultsNavigationArgs(Event @event, Run run)
        {
            Event = @event;
            Run = run;
        }
    }
}
