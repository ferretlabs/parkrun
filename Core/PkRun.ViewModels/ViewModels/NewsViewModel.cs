﻿using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using PkRun.Collections;
using PkRun.Command;
using PkRun.Core.Services;
using PkRun.ViewModels.Items;
using INavigationService = PkRun.Core.Services.INavigationService;

namespace PkRun.ViewModels
{
    public class NewsViewModel : PageBaseViewModel
    {
        private readonly INavigationService _navigationService;
        private readonly INewsApiHandler _newsApiHandler;
        private readonly ICancellationManager _cancellationManager;

        private int _eventId;

        public NewsViewModel(
            INavigationService navigationService,
            INewsApiHandler newsApiHandler,
            ICancellationManager cancellationManager)
        {
            _navigationService = navigationService;
            _newsApiHandler = newsApiHandler;
            _cancellationManager = cancellationManager;

            NewsItems = new NewsCollection(new NewsSource(_newsApiHandler, _navigationService, _cancellationManager));

            CheckForNewItemsCommand = new AsyncRelayCommand(CheckForNewItems);
        }

        public NewsCollection NewsItems { get; set; }

        public AsyncRelayCommand CheckForNewItemsCommand { get; }

        public void SetEventId(int eventId)
        {
            _eventId = eventId;
            NewsItems.EventId = _eventId;
        }

        public override Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            SetEventId((int) eventArgs.Parameter);

            return base.OnNavigatedToAsync(eventArgs);
        }

        private async Task CheckForNewItems()
        {
            var token = _cancellationManager.CurrentCancellationToken;
            var items = await _newsApiHandler.GetLatestNewsItems(_eventId, token);

            foreach (var item in items)
            {
                NewsItems.Insert(0, new NewsItemViewModel(item, _newsApiHandler, _navigationService, _cancellationManager));
            }
        }
    }
}
