﻿using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using ParkRunPortable;
using PkRun.Core.Services;
using PkRun.ViewModels.Items;
using INavigationService = PkRun.Core.Services.INavigationService;

namespace PkRun.ViewModels
{
    public class MainViewModel : PageBaseViewModel
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly INavigationService _navigationService;
        private readonly IParkRunClient _parkRunClient;

        public MainViewModel(
            IAuthenticationService authenticationService, 
            INavigationService navigationService,
            IParkRunClient parkRunClient)
        {
            _authenticationService = authenticationService;
            _navigationService = navigationService;
            _parkRunClient = parkRunClient;

            Athlete = new AthleteItemViewModel(_authenticationService.SignedInAthlete, _parkRunClient, _authenticationService);
        }

        public AthleteItemViewModel Athlete { get; set; }

        public string Welcome => $"Welcome to PkRun, {Athlete?.FirstName}";

        public override Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            return base.OnNavigatedToAsync(eventArgs);
        }
    }
}