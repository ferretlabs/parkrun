﻿using System.Net;
using System.Threading.Tasks;
using ParkRunPortable.Model;
using PkRun.Command;
using PkRun.Core.Services;

namespace PkRun.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly INavigationService _navigationService;
        private readonly IAuthenticationService _authenticationService;

        public LoginViewModel(INavigationService navigationService,
            IAuthenticationService authenticationService)
        {
            _navigationService = navigationService;
            _authenticationService = authenticationService;

            LoginCommand = new AsyncRelayCommand(Login);
        }

        public string Username { get; set; } = "A3222517";
        public string Password { get; set; } = "e4Y$82EJw@MZ";

        public string ErrorMessage { get; set; }
        public bool ShowErrorMessage => !string.IsNullOrEmpty(ErrorMessage);

        public bool CanSignIn => !string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password);

        public AsyncRelayCommand LoginCommand { get; }

        private async Task Login()
        {
            if (!CanSignIn)
            {
                return;
            }

            ErrorMessage = null;

            try
            {
                await _authenticationService.SignIn(Username, Password);

                //await _navigationService.NavigateToHome();
                //await _navigationService.ClearBackstack();
            }
            catch (ParkRunException ex)
            {
                switch (ex.StatusCode)
                {
                    case HttpStatusCode.Unauthorized:
                        ErrorMessage = "Error signing in, please check your username and password";
                        break;
                    default:
                        ErrorMessage = "There was an error signing you in";
                        break;
                }
            }
        }
    }
}
