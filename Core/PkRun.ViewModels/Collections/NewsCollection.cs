﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PkRun.Core.Collections;
using PkRun.Core.Services;
using PkRun.ViewModels.Items;

namespace PkRun.Collections
{
    public class NewsSource : IIncrementalSource<NewsItemViewModel>
    {
        private readonly INewsApiHandler _newsApiHandler;
        private readonly INavigationService _navigationService;
        private readonly ICancellationManager _cancellationManager;

        public NewsSource(
            INewsApiHandler newsApiHandler, 
            INavigationService navigationService,
            ICancellationManager cancellationManager)
        {
            _newsApiHandler = newsApiHandler;
            _navigationService = navigationService;
            _cancellationManager = cancellationManager;
        }

        public int EventId { get; set; }

        public async Task<IEnumerable<NewsItemViewModel>> GetPagedItemsAsync(int pageIndex, int pageSize, CancellationToken cancellationToken = new CancellationToken())
        {
            var offset = pageIndex * pageSize;

            var response = await _newsApiHandler.GetNewsItems(EventId, offset, cancellationToken);
            return response.Select(x => new NewsItemViewModel(x, _newsApiHandler, _navigationService, _cancellationManager));
        }
    }

    public class NewsCollection : IncrementalLoadingCollection<NewsSource, NewsItemViewModel>
    {

        public NewsCollection(NewsSource source)
            : base(source, 10)
        {
        }

        public int EventId
        {
            get { return Source.EventId; }
            set { Source.EventId = value; }
        }
    }
}
