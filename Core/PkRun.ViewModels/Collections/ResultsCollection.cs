﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ParkRunPortable;
using ParkRunPortable.Model;
using ParkRunPortable.Model.Requests;
using PkRun.Core.Collections;
using PkRun.Core.Extensions;
using PkRun.Core.Services;
using PkRun.ViewModels.Items;

namespace PkRun.Collections
{
    public class ResultsSource : IIncrementalSource<ResultItemViewModel>
    {
        private readonly IParkRunClient _parkRunClient;
        private readonly INavigationService _navigationService;
        private readonly IAuthenticationService _authenticationService;

        public ResultsSource(
            IParkRunClient parkRunClient,
            INavigationService navigationService,
            IAuthenticationService authenticationService)
        {
            _parkRunClient = parkRunClient;
            _navigationService = navigationService;
            _authenticationService = authenticationService;
        }

        public int EventId { get; private set; }
        public DateTimeOffset Date { get; private set; }
        public int RunId { get; set; }

        internal void SetCriteria(int eventId, int runId, DateTimeOffset date)
        {
            EventId = eventId;
            Date = date;
            RunId = runId;
        }

        public async Task<IEnumerable<ResultItemViewModel>> GetPagedItemsAsync(int pageIndex, int pageSize, CancellationToken cancellationToken = new CancellationToken())
        {
            try
            {
                var offset = pageIndex * pageSize;
                var response = await _parkRunClient.GetResultsForRunAsync(EventId, RunId, Date, new FilterOptions {Limit = pageSize, Offset = offset, ExpandedDetails = true}, cancellationToken: cancellationToken);
                return response.Items.Select(x => new ResultItemViewModel(x, _navigationService, _authenticationService));
            }
            catch (ParkRunException)
            {
                return Enumerable.Empty<ResultItemViewModel>();
            }
            catch (Exception ex)
            {
                return Enumerable.Empty<ResultItemViewModel>();
            }
        }
    }

    public class ResultsCollection : IncrementalLoadingCollection<ResultsSource, ResultItemViewModel>
    {
        public ResultsCollection(ResultsSource resultSource)
            : base(resultSource, 100)
        {
        }

        public ResultsSource GetSource() => Source;

        public void SetRunInformation(Run run)
        {
            Source.SetCriteria(run.EventNumber, run.RunId, run.EventDate);
            RefreshAsync().DontAwait();
        }
    }
}
