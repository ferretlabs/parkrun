﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ParkRunPortable.Extensions;
using ParkRunPortable.Http;
using ParkRunPortable.Logging;
using ParkRunPortable.Model;
using ParkRunPortable.Model.Requests;
using ParkRunPortable.Model.Responses;
using ParkRunPortable.Model.Responses.Internal;

namespace ParkRunPortable
{
    public interface IParkRunClient
    {
        Token Token { get; }
        event EventHandler<Token> TokenUpdated;
        void SetToken(Token token);
        Task<Token> AuthenticateAsync(string username, string password, CancellationToken cancellationToken = default(CancellationToken));
        Task<Token> RefreshTokenAsync(string refreshToken, CancellationToken cancellationToken = default(CancellationToken));
        Task<Athlete> GetAthleteProfileAsync(int athleteId, bool expandedDetails = false, CancellationToken cancellationToken = default(CancellationToken));
        Task<Athlete> GetAthleteProfileAsync(string username, bool expandedDetails = false, CancellationToken cancellationToken = default(CancellationToken));
        Task<int> GetAthleteRunCountAsync(int athleteId, CancellationToken cancellationToken = default(CancellationToken));

        Task<ResultsResponse> GetRunsForAthleteAsync(int athleteId, FilterOptions filterOptions = null, CancellationToken cancellationToken = default(CancellationToken));

        Task<Event> GetEventAsync(int eventNumber, CancellationToken cancellationToken = default(CancellationToken));
        Task<NewsResponse> GetNewsForEventAsync(int eventNumber, FilterOptions filterOptions = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<NewsItem> GetNewsItemAsync(int eventNumber, int newsId, CancellationToken cancellationToken = default(CancellationToken));
        Task<CourseDataResponse> GetCourseDataAsync(int eventNumber, CancellationToken cancellationToken = default(CancellationToken));
        Task<string> GetCourseRouteMapAsync(int eventNumber, CancellationToken cancellationToken = default(CancellationToken));
        Task<RunsResponse> GetRunsForEventAsync(int eventNumber, FilterOptions filterOptions = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<ResultsResponse> GetResultsForRunAsync(int eventNumber, int runId, DateTimeOffset dateOfRun, FilterOptions filterOptions = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<CountriesResponse> GetCountriesAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<ClubsResponse> GetClubsByCountryAsync(int countryCode, FilterOptions filterOptions = null, CancellationToken cancellationToken = default(CancellationToken));

        Task<AthletesResponse> SearchUsersAsync(
            string firstName,
            string lastName,
            int? countryCode = null,
            int? clubId = null,
            FilterOptions filterOptions = null,
            CancellationToken cancellationToken = default(CancellationToken));

        Task<int> SendFollowRequestAsync(int athleteId, CancellationToken cancellationToken = default(CancellationToken));
    }

    public class ParkRunClient : IParkRunClient
    {
        private readonly IClientHandlerFactory _handlerFactory;
        private readonly ILogger _logger;

        private const string ApiUrl = "https://api.parkrun.com/";
        private const string BaseUrl = ApiUrl + "v1/";

        private HttpClient _httpClient;
        private HttpClientHandler _handler;

        public Token Token { get; private set; }

        public event EventHandler<Token> TokenUpdated;

        public ParkRunClient(IClientHandlerFactory handlerFactory, ILogger logger)
        {
            _handlerFactory = handlerFactory;
            _handler = GetHandlerFromFactory(handlerFactory);

            _httpClient = CreateHttpClient();
            _logger = logger ?? new NullLogger();
        }

        public ParkRunClient(IClientHandlerFactory handlerFactory)
            : this(handlerFactory, new NullLogger())
        {
        }

        public ParkRunClient()
            : this(null)
        {
        }

        private static HttpClientHandler GetHandlerFromFactory(IClientHandlerFactory handlerFactory)
        {
            return handlerFactory?.CreateHandler() as HttpClientHandler;
        }

        private HttpClient CreateHttpClient()
        {
            return _handler != null ? new HttpClient(_handler) : new HttpClient();
        }

        private void HandlerNeedsRecreating()
        {
            var clientHandler = _handler as IClientHandler;
            if (clientHandler == null || !clientHandler.IsDisposed)
            {
                return;
            }

            _handler = GetHandlerFromFactory(_handlerFactory);
            _httpClient = CreateHttpClient();
        }

        private HttpClient CreateAuthorisationHttpClient()
        {
            HandlerNeedsRecreating();

            var client = CreateHttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Basic bmV0ZHJlYW1zLWlwaG9uZS1zMDE6Z2ZLYkRENk5Ka1lvRm1raXNSKGlWRm9wUUNLV3piUWVRZ1pBWlpLSw==");

            return client;
        }

        public void SetToken(Token token)
        {
            if (token?.AccessToken != Token?.AccessToken)
            {
                Token = token;
                TokenUpdated?.Invoke(this, Token);
            }
        }

        public async Task<Token> AuthenticateAsync(string username, string password, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException(nameof(username), "Username cannot be empty");
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException(nameof(password), "Password cannot be empty");
            }

            using (var client = CreateAuthorisationHttpClient())
            {
                var options = new Dictionary<string, string>
                {
                    {"scope", "app"},
                    {"grant_type", "password"},
                    {"password", password},
                    {"username", username}
                };

                var url = ApiUrl + "user_auth.php";

                var response = await client.PostAsync(url, new FormUrlEncodedContent(options), cancellationToken);

                var result = await HandleResponse<Authorisation>(response);

                var token = result.ToToken();
                SetToken(token);

                return token;
            }
        }

        public async Task<Token> RefreshTokenAsync(string refreshToken, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(refreshToken))
            {
                throw new ArgumentNullException(nameof(refreshToken), "Refresh token cannot be null or empty");
            }

            var url = ApiUrl + "refresh.php";
            var options = new Dictionary<string, string>
            {
                {"grant_type", "refresh_token"},
                {"refresh_token", refreshToken}
            };

            using (var client = CreateAuthorisationHttpClient())
            {
                var response = await client.PostAsync(url, new FormUrlEncodedContent(options), cancellationToken);
                var result = await HandleResponse<Authorisation>(response);
                result.RefreshToken = refreshToken;

                var token = result.ToToken();
                SetToken(token);

                return token;
            }
        }

        public Task<Athlete> GetAthleteProfileAsync(int athleteId, bool expandedDetails = false, CancellationToken cancellationToken = default(CancellationToken))
        {
            return GetAthleteProfileAsync(athleteId.ToString(), expandedDetails, cancellationToken);
        }

        public async Task<Athlete> GetAthleteProfileAsync(string username, bool expandedDetails = false, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException(nameof(username), "Username cannot be empty");
            }

            await CheckIfAuthenticated(cancellationToken);

            var options = new Dictionary<string, string> { { "expandedDetails", expandedDetails.ToString().ToLower() } };
            var response = await GetResponse<AthletesResponseInternal>("athletes", username, options, cancellationToken);
            return response.Data.Data.FirstOrDefault();
        }

        public async Task<int> GetAthleteRunCountAsync(int athleteId, CancellationToken cancellationToken = default(CancellationToken))
        {
            await CheckIfAuthenticated(cancellationToken);

            var options = new Dictionary<string, string>
            {
                {"athleteId", athleteId.ToString()}
            };

            var response = await GetResponse<TotalsResponseInternal>("hasrun", "count/Run", options, cancellationToken);
            return response.Data?.Data?.FirstOrDefault()?.RunTotal ?? 0;
        }

        public async Task<ResultsResponse> GetRunsForAthleteAsync(int athleteId, FilterOptions filterOptions = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            await CheckIfAuthenticated(cancellationToken);

            var options = new Dictionary<string, string>
            {
                { "athleteId", athleteId.ToString() },
                { "limit", filterOptions?.Limit?.ToString() ?? "10" },
                { "offset", filterOptions?.Offset?.ToString() ?? "0" }
            };

            options.AddIfNotNull("Updated_dateAfter", filterOptions?.GetDate(d => d.UpdatedDate));
            options.AddIfNotNull("EventDate_dateAfter", filterOptions?.GetDate(d => d.AfterDate));
            options.AddIfNotNull("EventDate_dateBefore", filterOptions?.GetDate(d => d.BeforeDate));

            var response = await GetResponse<ResultsResponseInternal>("results", string.Empty, options, cancellationToken);
            return response.ToPublicResponse();
        }

        public async Task<Event> GetEventAsync(int eventNumber, CancellationToken cancellationToken = default(CancellationToken))
        {
            await CheckIfAuthenticated(cancellationToken);

            var response = await GetResponse<EventsResponseInternal>("events", eventNumber.ToString(), new Dictionary<string, string>(), cancellationToken);
            return response.Data.Data.FirstOrDefault();
        }

        public async Task<NewsResponse> GetNewsForEventAsync(int eventNumber, FilterOptions filterOptions = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            await CheckIfAuthenticated(cancellationToken);

            var options = new Dictionary<string, string>
            {
                { "limit", filterOptions?.Limit?.ToString() ?? "10" },
                { "offset", filterOptions?.Offset?.ToString() ?? "0" }
            };

            var response = await GetResponse<NewsResponseInternal>("news", eventNumber.ToString(), options, cancellationToken);
            return response.ToPublicResponse();
        }

        public async Task<NewsItem> GetNewsItemAsync(int eventNumber, int newsId, CancellationToken cancellationToken = default(CancellationToken))
        {
            await CheckIfAuthenticated(cancellationToken);

            var response = await GetResponse<NewsResponseInternal>("news", $"{eventNumber}/{newsId}", null, cancellationToken);
            return response.Data.Data.FirstOrDefault();
        }

        public async Task<CourseDataResponse> GetCourseDataAsync(int eventNumber, CancellationToken cancellationToken = default(CancellationToken))
        {
            await CheckIfAuthenticated(cancellationToken);

            var options = new Dictionary<string, string> { { "eventNumber", eventNumber.ToString() } };

            var response = await GetResponse<CourseDataResponseInternal>("courseData", string.Empty, options, cancellationToken);
            return response.ToPublicResponse();
        }

        public async Task<string> GetCourseRouteMapAsync(int eventNumber, CancellationToken cancellationToken = default(CancellationToken))
        {
            await CheckIfAuthenticated(cancellationToken);

            var response = await GetResponse<CourseMapResponseInternal>("events", $"{eventNumber}/courseMaps", null, cancellationToken);
            return response?.Data.Data.FirstOrDefault()?.Value;
        }

        public async Task<RunsResponse> GetRunsForEventAsync(int eventNumber, FilterOptions filterOptions = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            await CheckIfAuthenticated(cancellationToken);

            var options = new Dictionary<string, string>
            {
                { "eventNumber", eventNumber.ToString() },
                { "limit", filterOptions?.Limit?.ToString() ?? "10" },
                { "offset", filterOptions?.Offset?.ToString() ?? "0" }
            };

            options.AddIfNotNull(filterOptions?.GetSortKey(), filterOptions?.GetSortFields());

            var response = await GetResponse<RunsResponseInternal>("runs", string.Empty, options, cancellationToken);
            return response.ToPublicResponse();
        }

        public async Task<ResultsResponse> GetResultsForRunAsync(int eventNumber, int runId, DateTimeOffset dateOfRun, FilterOptions filterOptions = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            await CheckIfAuthenticated(cancellationToken);

            var options = new Dictionary<string, string>
            {
                { "runId", runId.ToString() },
                { "eventNumber", eventNumber.ToString() },
                { "EventDate", dateOfRun.ToParkRunDate() },
                { "limit", filterOptions?.Limit?.ToString() ?? "10" },
                { "offset", filterOptions?.Offset?.ToString() ?? "0" },
                { "expandedDetails", (filterOptions?.ExpandedDetails ?? false).ToString().ToLower() }
            };

            var response = await GetResponse<ResultsResponseInternal>("results", string.Empty, options, cancellationToken);
            return response.ToPublicResponse();
        }

        public async Task<CountriesResponse> GetCountriesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            await CheckIfAuthenticated(cancellationToken);

            var response = await GetResponse<CountriesResponseInternal>("countries", string.Empty, null, cancellationToken);
            return response.ToPublicResponse();
        }

        public async Task<ClubsResponse> GetClubsByCountryAsync(int countryCode, FilterOptions filterOptions = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            await CheckIfAuthenticated(cancellationToken);

            var options = new Dictionary<string, string>
            {
                { "countryCode", countryCode.ToString() },
                { "limit", filterOptions?.Limit?.ToString() ?? "10" },
                { "offset", filterOptions?.Offset?.ToString() ?? "0" }
            };

            var response = await GetResponse<ClubsResponseInternal>("clubs", string.Empty, options, cancellationToken);
            return response.ToPublicResponse();
        }

        public async Task<AthletesResponse> SearchUsersAsync(
            string firstName,
            string lastName,
            int? countryCode = null,
            int? clubId = null,
            FilterOptions filterOptions = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(firstName))
            {
                throw new ArgumentNullException(nameof(firstName), "First name cannot be null");
            }

            if (string.IsNullOrEmpty(lastName))
            {
                throw new ArgumentNullException(nameof(lastName), "Last name cannot be null or empty");
            }

            await CheckIfAuthenticated(cancellationToken);

            var options = new Dictionary<string, string>
            {
                { "limit", filterOptions?.Limit?.ToString() ?? "10" },
                { "offset", filterOptions?.Offset?.ToString() ?? "0" }
            };
            options.AddIfNotNull("countryCode", countryCode);
            options.AddIfNotNull("clubId", clubId);

            var response = await GetResponse<AthletesResponseInternal>("searchAthletes", $"{firstName}/{lastName}", options, cancellationToken);
            return response.ToPublicResponse();
        }

        public async Task<int> SendFollowRequestAsync(int athleteId, CancellationToken cancellationToken = default(CancellationToken))
        {
            await CheckIfAuthenticated(cancellationToken);

            var options = new Dictionary<string, string>
            {
                { "FollowsId", athleteId.ToString() },
                { "FollowsType", "Athlete" }
            };

            var response = await PostResponse<FollowsResponseInternal>("follows", string.Empty, options, cancellationToken);
            return response.Data.Data.FirstOrDefault()?.Id ?? 0;
        }

        #region Private methods

        private async Task CheckIfAuthenticated(CancellationToken cancellationToken)
        {
            if (Token == null)
            {
                throw new UnauthorizedAccessException("You have to sign in first before you can make this call");
            }

            var now = DateTimeOffset.Now;
            if (now > Token.ExpiresAt)
            {
                await RefreshTokenAsync(Token.RefreshToken, cancellationToken);
            }
        }

        private async Task<TResponseType> PostMultiPartResponse<TResponseType>(string endPoint, string method, Stream data, string filename, Dictionary<string, string> options = null, CancellationToken cancellationToken = default(CancellationToken), [CallerMemberName] string callingMethod = "")
            where TResponseType : BaseResponseInternal
        {
            _logger.Debug(callingMethod);
            var url = $"{BaseUrl}{endPoint}/{method}";

            if (options != null && options.Any())
            {
                var queryString = options.ToQueryString();
                url = $"{url}?{queryString}";
            }

            var requestTime = DateTime.Now;

            cancellationToken.ThrowParkRunExceptionIfCancelled();

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                await data.CopyToAsync(stream).ConfigureAwait(false);
                bytes = stream.ToArray();
            }

            cancellationToken.ThrowParkRunExceptionIfCancelled();

            HttpResponseMessage response;
            using (var content = new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
            {
                var imageContent = new ByteArrayContent(bytes);
                imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");
                content.Add(imageContent, "file", filename);

                HandlerNeedsRecreating();

                response = await _httpClient.PostAsync(url, content, cancellationToken).ConfigureAwait(false);
            }

            cancellationToken.ThrowParkRunExceptionIfCancelled();

            var duration = DateTime.Now - requestTime;

            cancellationToken.ThrowParkRunExceptionIfCancelled();

            _logger.Debug("Received {0} status code after {1} ms from {2}: {3}", response.StatusCode, duration.TotalMilliseconds, "POST", url);

            return await HandleBaseResponse<TResponseType>(response).ConfigureAwait(false);
        }

        private async Task<TResponseType> PostResponse<TResponseType>(string endPoint, string method, Dictionary<string, string> postData = null, CancellationToken cancellationToken = default(CancellationToken), [CallerMemberName] string callingMethod = "")
            where TResponseType : BaseResponseInternal
        {
            if (postData == null)
            {
                postData = new Dictionary<string, string>
                {
                    {"access_token", Token.AccessToken}
                };
            }
            else
            {
                postData.Add("access_token", Token.AccessToken);
            }

            _logger.Debug(callingMethod);
            var url = $"{BaseUrl}{endPoint}/{method}";
            url = url.TrimEnd('/');

            var requestTime = DateTime.Now;

            var parameters = postData.ToQueryString();

            cancellationToken.ThrowParkRunExceptionIfCancelled();

            HandlerNeedsRecreating();

            var response = await _httpClient.PostAsync(url, new StringContent(parameters), cancellationToken).ConfigureAwait(false);

            var duration = DateTime.Now - requestTime;

            cancellationToken.ThrowParkRunExceptionIfCancelled();

            _logger.Debug("Received {0} status code after {1} ms from {2}: {3}", response.StatusCode, duration.TotalMilliseconds, "POST", url);

            return await HandleBaseResponse<TResponseType>(response).ConfigureAwait(false);
        }

        private async Task<TResponseType> PutResponse<TResponseType>(string endPoint, string method, Dictionary<string, string> options = null, CancellationToken cancellationToken = default(CancellationToken), [CallerMemberName] string callingMethod = "")
            where TResponseType : BaseResponseInternal
        {
            _logger.Debug(callingMethod);
            var url = $"{BaseUrl}{endPoint}/{method}";
            if (options != null && options.Any())
            {
                var queryString = options.ToQueryString();
                url = $"{url}?{queryString}";
            }
            var requestTime = DateTime.Now;

            cancellationToken.ThrowParkRunExceptionIfCancelled();

            HandlerNeedsRecreating();

            var response = await _httpClient.PutAsync(url, new StringContent(string.Empty), cancellationToken).ConfigureAwait(false);

            cancellationToken.ThrowParkRunExceptionIfCancelled();

            var duration = DateTime.Now - requestTime;
            _logger.Debug("Received {0} status code after {1} ms from {2}: {3}", response.StatusCode, duration.TotalMilliseconds, "PUT", url);

            return await HandleBaseResponse<TResponseType>(response).ConfigureAwait(false);
        }

        private async Task<TResponseType> PutJsonResponse<TResponseType>(string endPoint, string method, object postData, CancellationToken cancellationToken = default(CancellationToken), [CallerMemberName] string callingMethod = "")
            where TResponseType : BaseResponseInternal
        {
            _logger.Debug(callingMethod);
            var url = $"{BaseUrl}{endPoint}/{method}";
            var requestTime = DateTime.Now;

            var json = JsonConvert.SerializeObject(postData);

            cancellationToken.ThrowParkRunExceptionIfCancelled();

            HandlerNeedsRecreating();

            var response = await _httpClient.PutAsync(url, new StringContent(json, Encoding.UTF8, "application/json"), cancellationToken).ConfigureAwait(false);

            var duration = DateTime.Now - requestTime;
            _logger.Debug("Received {0} status code after {1} ms from {2}: {3}", response.StatusCode, duration.TotalMilliseconds, "PUT", url);

            return await HandleBaseResponse<TResponseType>(response).ConfigureAwait(false);
        }

        private async Task<TResponseType> GetResponse<TResponseType>(string endPoint, string method, Dictionary<string, string> options = null, CancellationToken cancellationToken = default(CancellationToken), [CallerMemberName] string callingMethod = "")
            where TResponseType : BaseResponseInternal

        {
            _logger.Debug(callingMethod);
            var url = $"{BaseUrl}{endPoint}/{method}";
            url = url.TrimEnd('/');

            if (options == null)
            {
                options = new Dictionary<string, string>
                {
                    {"access_token", Token.AccessToken}
                };
            }
            else
            {
                options.Add("access_token", Token.AccessToken);
            }

            var queryString = options.ToQueryString();
            url = $"{url}?{queryString}";

            _logger.Debug("GET: {0}", url);
            var requestTime = DateTime.Now;

            cancellationToken.ThrowParkRunExceptionIfCancelled();

            HandlerNeedsRecreating();

            var response = await _httpClient.GetAsync(url, cancellationToken).ConfigureAwait(false);
            var duration = DateTime.Now - requestTime;

            cancellationToken.ThrowParkRunExceptionIfCancelled();

            _logger.Debug("Received {0} status code after {1} ms from {2}: {3}", response.StatusCode, duration.TotalMilliseconds, "GET", url);

            return await HandleBaseResponse<TResponseType>(response).ConfigureAwait(false);
        }

        private async Task<TResponseType> DeleteResponse<TResponseType>(string endPoint, string method, Dictionary<string, string> options = null, CancellationToken cancellationToken = default(CancellationToken), [CallerMemberName] string callingMethod = "")
            where TResponseType : BaseResponseInternal
        {
            _logger.Debug(callingMethod);
            var url = $"{BaseUrl}{endPoint}/{method}";

            if (options != null && options.Any())
            {
                var queryString = options.ToQueryString();
                url = $"{url}?{queryString}";
            }

            _logger.Debug("DELETE: {0}", url);
            var requestTime = DateTime.Now;

            cancellationToken.ThrowParkRunExceptionIfCancelled();

            HandlerNeedsRecreating();

            var response = await _httpClient.DeleteAsync(url, cancellationToken).ConfigureAwait(false);
            var duration = DateTime.Now - requestTime;

            cancellationToken.ThrowParkRunExceptionIfCancelled();

            _logger.Debug("Received {0} status code after {1} ms from {2}: {3}", response.StatusCode, duration.TotalMilliseconds, "DELETE", url);

            return await HandleBaseResponse<TResponseType>(response).ConfigureAwait(false);
        }

        private static async Task<TResponseType> HandleBaseResponse<TResponseType>(HttpResponseMessage response) where TResponseType : BaseResponseInternal
        {
            var responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            if (!response.IsSuccessStatusCode)
            {
                var jsonSettings = new JsonSerializerSettings { Error = (sender, args) => args.ErrorContext.Handled = true };
                var error = JsonConvert.DeserializeObject<ErrorResponse>(responseString, jsonSettings);
                if (error != null)
                {
                    throw new ParkRunException(error.ErrorDescription);
                }

                var baseError = JsonConvert.DeserializeObject<TResponseType>(responseString, jsonSettings);
                if (baseError != null)
                {
                    throw new ParkRunException(baseError.Error.ErrorMessage, baseError.Error.HttpStatusCode);
                }
            }

            var item = await responseString.DeserialiseAsync<TResponseType>().ConfigureAwait(false);
            return item;
        }

        private static async Task<TResponseType> HandleResponse<TResponseType>(HttpResponseMessage response)
        {
            var responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            if (!response.IsSuccessStatusCode)
            {
                var error = JsonConvert.DeserializeObject<ErrorResponse>(responseString);
                if (error != null)
                {
                    throw new ParkRunException(error.ErrorDescription);
                }
            }

            var item = await responseString.DeserialiseAsync<TResponseType>().ConfigureAwait(false);
            return item;
        }

        #endregion
    }
}
