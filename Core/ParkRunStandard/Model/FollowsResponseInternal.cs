﻿using System.Collections.Generic;
using Newtonsoft.Json;
using ParkRunPortable.Converters;
using ParkRunPortable.Model.Responses;

namespace ParkRunPortable.Model
{
    internal class FollowsRange : IRangeData
    {
        [JsonProperty("FollowsRange")]
        public Range[] Range { get; set; }
    }

    internal class FollowsData : IData<Follows>
    {
        [JsonProperty("Follows")]
        [JsonConverter(typeof(ListConverter<Follows>))]
        public List<Follows> Data { get; set; }
    }

    internal class FollowsResponseInternal : BaseDataResponse<FollowsData, FollowsRange, FollowsResponse, Follows>
    {
    }

    public class FollowsResponse : BaseResponse<Follows>
    {
        public FollowsResponse(List<Follows> items, int maxItems) 
            : base(items, maxItems)
        {
        }
    }
}
