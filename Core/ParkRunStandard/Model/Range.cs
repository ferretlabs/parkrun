﻿using Newtonsoft.Json;

namespace ParkRunPortable.Model
{
    public class Range
    {
        internal Range() { }

        [JsonProperty("first")]
        public int First { get; set; }

        [JsonProperty("last")]
        public int Last { get; set; }

        [JsonProperty("max")]
        public int Max { get; set; }
    }
}