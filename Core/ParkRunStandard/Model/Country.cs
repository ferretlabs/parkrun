﻿using Newtonsoft.Json;
using ParkRunPortable.Converters;

namespace ParkRunPortable.Model
{
    public class Country
    {
        [JsonProperty("CountryCode")]
        public int CountryCode { get; set; }

        [JsonProperty("Country")]
        public string Name { get; set; }

        [JsonProperty("Active")]
        [JsonConverter(typeof(BooleanJsonConverter))]
        public bool Active { get; set; }

        [JsonProperty("CountrySiteUrl")]
        public string CountrySiteUrl { get; set; }

        [JsonProperty("DefaultLanguageId")]
        public int? DefaultLanguageId { get; set; }

        [JsonProperty("wikiCountryName")]
        public string WikiCountryName { get; set; }

        [JsonProperty("ccTLD")]
        public string CcTld { get; set; }
    }
}