using System;
using Newtonsoft.Json;
using ParkRunPortable.Converters;

namespace ParkRunPortable.Model
{
    public class Event
    {
        [JsonProperty("EventNumber")]
        public int EventNumber { get; set; }

        [JsonProperty("EventName")]
        public string EventName { get; set; }

        [JsonProperty("EventShortName")]
        public string EventShortName { get; set; }

        [JsonProperty("EventLongName")]
        public string EventLongName { get; set; }

        [JsonProperty("EventLocation")]
        public string EventLocation { get; set; }

        [JsonProperty("CountryCode")]
        public int CountryCode { get; set; }

        [JsonProperty("PreferredLanguage")]
        public string PreferredLanguage { get; set; }

        [JsonProperty("SeriesID")]
        public int SeriesId { get; set; }

        [JsonProperty("NextAnniversary")]
        public DateTimeOffset NextAnniversary { get; set; }

        [JsonProperty("HomeRunSelection")]
        [JsonConverter(typeof(BooleanJsonConverter))]
        public bool HomeRunSelection { get; set; }

        [JsonProperty("StatusLive")]
        [JsonConverter(typeof(BooleanJsonConverter))]
        public bool StatusLive { get; set; }

        [JsonProperty("AnniversarySaturdayOfMonth")]
        public string AnniversarySaturdayOfMonth { get; set; }

        [JsonProperty("EventStatus")]
        public string EventStatus { get; set; }

        [JsonProperty("UserFavourite")]
        public object UserFavourite { get; set; }

        public override bool Equals(object obj)
        {
            var e = obj as Event;

            return e?.EventNumber == EventNumber;
        }
    }
}