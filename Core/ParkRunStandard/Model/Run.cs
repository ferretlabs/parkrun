﻿using System;
using System.Diagnostics;
using Newtonsoft.Json;

namespace ParkRunPortable.Model
{
    [DebuggerDisplay("Date: {EventDate}, ID: {RunId}")]
    public class Run
    {
        [JsonProperty("EventNumber")]
        public int EventNumber { get; set; }

        [JsonProperty("RunId")]
        public int RunId { get; set; }

        [JsonProperty("NumberRunners")]
        public int NumberRunners { get; set; }

        [JsonProperty("EventDate")]
        public DateTimeOffset EventDate { get; set; }

        [JsonProperty("NumberOfVolunteers")]
        public int NumberOfVolunteers { get; set; }
    }
}