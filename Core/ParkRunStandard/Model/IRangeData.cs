﻿namespace ParkRunPortable.Model
{
    internal interface IRangeData
    {
        Range[] Range { get; set; }
    }
}