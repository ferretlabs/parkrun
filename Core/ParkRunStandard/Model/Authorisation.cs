﻿using System;
using Newtonsoft.Json;

namespace ParkRunPortable.Model
{
    internal class Authorisation
    {
        private DateTimeOffset _now;

        internal Authorisation()
        {
            _now = DateTimeOffset.Now;
        }

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("scope")]
        public string Scope { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        internal Token ToToken()
        {
            return new Token {AccessToken = AccessToken, RefreshToken = RefreshToken, ExpiresAt = _now + TimeSpan.FromSeconds(int.Parse(ExpiresIn))};
        }
    }

    public class Token
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTimeOffset ExpiresAt { get; set; }
    }
}
