namespace ParkRunPortable.Model
{
    public enum CourseDataMetaKey
    {
        CourseDescription,
        CourseFacilities,
        CourseIntro,
        EventCafe,
        EventName,
        StartTime,
        EventHelpersEmail,
        EventLocation,
        HomepageCafeParagraph,
        HomepageEventLocationDescription,
        LocationOfStartParagraph,
        DirectionsByPublicTransport,
        DirectionsOnFoot,
        DirectionsByCar,
        TheCoreVolunteers,
        EventOfficeEmail,
        EventSupporters,
        DogsMessage,
        EventPostcode
    }
}