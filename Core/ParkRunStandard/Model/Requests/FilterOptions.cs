﻿using System;
using System.Collections.Generic;
using System.Linq;
using ParkRunPortable.Extensions;

namespace ParkRunPortable.Model.Requests
{
    public class FilterOptions
    {
        public int? Limit { get; set; }
        public int? Offset { get; set; }
        public DateTimeOffset? AfterDate { get; set; }
        public DateTimeOffset? BeforeDate { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
        public SortOrder? SortOrder { get; set; }
        public List<string> OrderFields { get; set; }
        public bool? ExpandedDetails { get; set; }

        internal string GetSortKey()
        {
            var key = SortOrder == Requests.SortOrder.Ascending ? "OrderBy" : "OrderByDesc";
            return key;
        }

        internal string GetSortFields()
        {
            return !SortOrder.HasValue ? null : string.Join(",", OrderFields.Select(x => x));
        }

        internal string GetDate(Func<FilterOptions, DateTimeOffset?> getDateFunc)
        {
            var date = getDateFunc(this);
            return date?.ToParkRunDate();
        }
    }

    public enum SortOrder
    {
        Ascending,
        Descending
    }
}
