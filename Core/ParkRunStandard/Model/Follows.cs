﻿using Newtonsoft.Json;

namespace ParkRunPortable.Model
{
    public class Follows
    {
        [JsonProperty("Id")]
        public int Id { get; set; }
    }
}