using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ParkRunPortable.Converters;

namespace ParkRunPortable.Model
{
    [DebuggerDisplay("Key: {" + nameof(Metakey) + "}")]
    public class CourseDataInfo
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("metakey")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CourseDataMetaKey Metakey { get; set; }

        [JsonProperty("english_phrase")]
        public string EnglishPhrase { get; set; }

        [JsonProperty("website_only")]
        [JsonConverter(typeof(BooleanJsonConverter))]
        public bool WebsiteOnly { get; set; }
    }
}