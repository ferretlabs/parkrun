﻿using System;
using System.Net;

namespace ParkRunPortable.Model
{
    public class ParkRunException : Exception
    {
        public ParkRunException() { }
        public ParkRunException(string message)
            : base(message) { }
        public ParkRunException(string message, Exception innerException)
            : base(message, innerException) { }
        public ParkRunException(string message, HttpStatusCode statusCode)
            : base(message)
        {
            StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; }
    }
}
