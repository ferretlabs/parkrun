﻿using System;
using Newtonsoft.Json;

namespace ParkRunPortable.Model
{
    public class Club
    {
        [JsonProperty("ClubID")]
        public int ClubId { get; set; }

        [JsonProperty("ClubName")]
        public string ClubName { get; set; }

        [JsonProperty("OrganisationID")]
        public int OrganisationId { get; set; }

        [JsonProperty("OrgSubTypeID")]
        public int? OrgSubTypeId { get; set; }

        [JsonProperty("ClubCaptain")]
        public string ClubCaptain { get; set; }

        [JsonProperty("ResultsCoordinator")]
        public string ResultsCoordinator { get; set; }

        [JsonProperty("Website")]
        public string Website { get; set; }

        [JsonProperty("NoMail")]
        public string NoMail { get; set; }

        [JsonProperty("CountryCode")]
        public int CountryCode { get; set; }

        [JsonProperty("Created")]
        public DateTimeOffset Created { get; set; }

        [JsonProperty("Last_Modified")]
        public DateTimeOffset LastModified { get; set; }
    }
}