using System;
using Newtonsoft.Json;
using ParkRunPortable.Converters;

namespace ParkRunPortable.Model
{
    public class Athlete
    {
        [JsonProperty("AthleteID")]
        public int AthleteId { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("HomeRunID")]
        public int HomeRunId { get; set; }

        [JsonProperty("Sex")]
        public string Sex { get; set; }

        [JsonProperty("DOB")]
        public DateTimeOffset? DateOfBirth { get; set; }

        [JsonProperty("eMailID")]
        public string EmailAddress { get; set; }

        [JsonProperty("OKtoMail")]
        [JsonConverter(typeof(BooleanJsonConverter))]
        public bool OKtoMail { get; set; }

        [JsonProperty("ClubID")]
        public int? ClubId { get; set; }

        [JsonProperty("ConfirmCode")]
        public string ConfirmCode { get; set; }

        [JsonProperty("CountryCode")]
        public int CountryCode { get; set; }

        [JsonProperty("WheelchairAthlete")]
        [JsonConverter(typeof(BooleanJsonConverter))]
        public bool WheelchairAthlete { get; set; }

        [JsonProperty("MobileNumber")]
        public string MobileNumber { get; set; }

        [JsonProperty("PreParkrunExerciseFrequency")]
        public string PreParkrunExerciseFrequency { get; set; }

        [JsonProperty("Postcode")]
        public string Postcode { get; set; }

        [JsonProperty("HomeRunName")]
        public string HomeRunName { get; set; }

        [JsonProperty("HomeRunLocation")]
        public string HomeRunLocation { get; set; }

        [JsonProperty("ClubName")]
        public string ClubName { get; set; }

        [JsonProperty("OrganisationID")]
        public int? OrganisationId { get; set; }

        [JsonProperty("OrgSubTypeID")]
        public int? OrgSubTypeId { get; set; }

        [JsonProperty("Avatar")]
        public string Avatar { get; set; }

        public override bool Equals(object obj)
        {
            var a = obj as Athlete;
            return a?.AthleteId == AthleteId;
        }
    }
}