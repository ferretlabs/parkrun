﻿using System.Collections.Generic;
using ParkRunPortable.Model.Responses.Internal;

namespace ParkRunPortable.Model.Responses
{
    public class TotalsResponse : BaseResponse<TotalRun>
    {
        public TotalsResponse(List<TotalRun> items, int maxItems) 
            : base(items, maxItems)
        {
        }
    }
}