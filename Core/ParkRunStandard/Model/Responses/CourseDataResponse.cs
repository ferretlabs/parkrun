using System.Collections.Generic;

namespace ParkRunPortable.Model.Responses
{
    public class CourseDataResponse : BaseResponse<CourseDataInfo>
    {
        public CourseDataResponse(List<CourseDataInfo> items, int maxItems)
            : base(items, maxItems)
        {
        }
    }
}