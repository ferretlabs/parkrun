using System.Collections.Generic;

namespace ParkRunPortable.Model.Responses
{
    public class ResultsResponse : BaseResponse<Result>
    {
        public ResultsResponse(List<Result> items, int maxItems) 
            : base(items, maxItems)
        {
        }
    }
}