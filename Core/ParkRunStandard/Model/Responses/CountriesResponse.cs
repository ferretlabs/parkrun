using System.Collections.Generic;

namespace ParkRunPortable.Model.Responses
{
    public class CountriesResponse : BaseResponse<Country>
    {
        public CountriesResponse(List<Country> items, int maxItems) 
            : base(items, maxItems)
        {
        }
    }
}