using System.Collections.Generic;

namespace ParkRunPortable.Model.Responses
{
    public class ClubsResponse : BaseResponse<Club>
    {
        public ClubsResponse(List<Club> items, int maxItems) 
            : base(items, maxItems)
        {
        }
    }
}