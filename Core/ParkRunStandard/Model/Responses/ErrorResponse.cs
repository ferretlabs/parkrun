﻿using System.Net;
using Newtonsoft.Json;

namespace ParkRunPortable.Model.Responses
{
    internal class ErrorResponse
    {
        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("code")]
        public HttpStatusCode HttpStatusCode { get; set; }

        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("human_message")]
        public string ErrorMessage { get; set; }
    }
}
