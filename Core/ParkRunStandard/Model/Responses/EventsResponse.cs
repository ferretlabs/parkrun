using System.Collections.Generic;

namespace ParkRunPortable.Model.Responses
{
    public class EventsResponse : BaseResponse<Event>
    {
        public EventsResponse(List<Event> items, int maxItems) 
            : base(items, maxItems)
        {
        }
    }
}