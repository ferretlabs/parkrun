﻿using System.Collections.Generic;

namespace ParkRunPortable.Model.Responses
{
    public class NewsResponse : BaseResponse<NewsItem>
    {
        public NewsResponse(List<NewsItem> items, int maxItems) 
            : base(items, maxItems)
        {
        }
    }
}