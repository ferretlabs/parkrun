using System.Collections.Generic;

namespace ParkRunPortable.Model.Responses
{
    public class RunsResponse : BaseResponse<Run>
    {
        public RunsResponse(List<Run> items, int maxItems) 
            : base(items, maxItems)
        {
        }
    }
}