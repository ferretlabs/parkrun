using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace ParkRunPortable.Model.Responses
{
    internal abstract class BaseResponseInternal
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("error")]
        public ErrorResponse Error { get; set; }

        [JsonProperty("timestamp")]
        public int Timestamp { get; set; }

        [JsonProperty("originalQryTime")]
        public int OriginalQryTime { get; set; }
    }

    internal abstract class BaseDataResponse<TDataType, TRangeType, TResponseType, TItemType> 
        : BaseResponseInternal
        where TResponseType : BaseResponse<TItemType>
        where TRangeType : IRangeData
        where TDataType : IData<TItemType>
    {
        [JsonProperty("Content-Range")]
        public TRangeType ContentRange { get; set; }

        [JsonProperty("data")]
        public TDataType Data { get; set; }

        public TResponseType ToPublicResponse()
        {
            var maxItems = ContentRange.Range.FirstOrDefault()?.Max ?? 0;
            return (TResponseType) Activator.CreateInstance(typeof(TResponseType), Data.Data, maxItems);
        }
    }

    public abstract class BaseResponse<TItemType>
    {
        public List<TItemType> Items { get; set; }
        public int MaxItems { get; set; }

        protected BaseResponse(List<TItemType> items, int maxItems)
        {
            Items = items;
            MaxItems = maxItems;
        }
    }
}