﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ParkRunPortable.Model.Responses.Internal
{
    internal class NewsRange : IRangeData
    {
        [JsonProperty("EventNewsRange")]
        public Range[] Range { get; set; }
    }

    internal class NewsData : IData<NewsItem>
    {
        [JsonProperty("EventNews")]
        public List<NewsItem> Data { get; set; }
    }

    internal class NewsResponseInternal : BaseDataResponse<NewsData, NewsRange, NewsResponse, NewsItem>
    {
    }
}
