﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ParkRunPortable.Model.Responses.Internal
{
    internal class EventData : IData<Event>
    {
        [JsonProperty("Events")]
        public List<Event> Data { get; set; }
    }

    internal class EventRange : IRangeData
    {
        [JsonProperty("EventsRange")]
        public Range[] Range { get; set; }
    }

    internal class EventsResponseInternal : BaseDataResponse<EventData, EventRange, EventsResponse, Event>
    {
    }
}