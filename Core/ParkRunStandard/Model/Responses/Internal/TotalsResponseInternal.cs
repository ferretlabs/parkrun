﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ParkRunPortable.Model.Responses.Internal
{
    public class TotalRun
    {
        [JsonProperty("RunTotal")]
        public int RunTotal { get; set; }
    }

    internal class TotalRunsRange : IRangeData
    {
        [JsonProperty("TotalRunsRange")]
        public Range[] Range { get; set; }
    }

    internal class TotalRunsData : IData<TotalRun>
    {
        [JsonProperty("TotalRuns")]
        public List<TotalRun> Data { get; set; }
    }

    internal class TotalsResponseInternal : BaseDataResponse<TotalRunsData, TotalRunsRange, TotalsResponse, TotalRun>
    {
    }
}
