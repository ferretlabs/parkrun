﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ParkRunPortable.Model.Responses.Internal
{
    internal class ClubRange : IRangeData
    {

        [JsonProperty("ClubsRange")]
        public Range[] Range { get; set; }
    }

    internal class ClubData : IData<Club>
    {
        [JsonProperty("Clubs")]
        public List<Club> Data { get; set; }
    }

    internal class ClubsResponseInternal : BaseDataResponse<ClubData, ClubRange, ClubsResponse, Club>
    {
    }
}
