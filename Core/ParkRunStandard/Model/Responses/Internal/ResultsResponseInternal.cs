﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ParkRunPortable.Model.Responses.Internal
{
    internal class ResultData : IData<Result>
    {
        [JsonProperty("Results")]
        public List<Result> Data { get; set; }
    }

    public class ResultRange : IRangeData
    {
        [JsonProperty("ResultsRange")]
        public Range[] Range { get; set; }
    }

    internal class ResultsResponseInternal : BaseDataResponse<ResultData, ResultRange, ResultsResponse, Result>
    {
    }
}
