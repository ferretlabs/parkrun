﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ParkRunPortable.Model.Responses.Internal
{
    internal class CountryRange : IRangeData
    {
        [JsonProperty("CountriesRange")]
        public Range[] Range { get; set; }
    }

    internal class CountryData : IData<Country>
    {
        [JsonProperty("Countries")]
        public List<Country> Data { get; set; }
    }

    internal class CountriesResponseInternal : BaseDataResponse<CountryData, CountryRange, CountriesResponse, Country>
    {
    }
}
