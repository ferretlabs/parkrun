﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ParkRunPortable.Model.Responses.Internal
{
    internal class CourseMapRange : IRangeData
    {
        [JsonProperty("CourseMapsRange")]
        public Range[] Range { get; set; }
    }

    internal class CourseMap
    {
        [JsonProperty("value")]
        public string Value { get; set; }
    }

    internal class CourseMapData : IData<CourseMap>
    {
        [JsonProperty("CourseMaps")]
        public List<CourseMap> Data { get; set; }
    }

    internal class CourseMapResponseInternal : BaseDataResponse<CourseMapData, CourseMapRange, CourseMapResponse, CourseMap>
    {
    }

    internal class CourseMapResponse : BaseResponse<CourseMap>
    {
        public CourseMapResponse(List<CourseMap> items, int maxItems)
            : base(items, maxItems)
        {
        }
    }
}
