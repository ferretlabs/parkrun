﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ParkRunPortable.Model.Responses.Internal
{
    internal class CourseDataRange : IRangeData
    {
        [JsonProperty("CourseDataRange")]
        public Range[] Range { get; set; }
    }

    internal class CourseData : IData<CourseDataInfo>
    {
        [JsonProperty("CourseData")]
        public List<CourseDataInfo> Data { get; set; }
    }

    internal class CourseDataResponseInternal : BaseDataResponse<CourseData, CourseDataRange, CourseDataResponse, CourseDataInfo>
    {
    }
}