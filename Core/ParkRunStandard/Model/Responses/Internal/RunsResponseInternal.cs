﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ParkRunPortable.Model.Responses.Internal
{
    internal class RunRange : IRangeData
    {
        [JsonProperty("RunsRange")]
        public Range[] Range { get; set; }
    }

    internal class RunData : IData<Run>
    {
        [JsonProperty("Runs")]
        public List<Run> Data { get; set; }
    }

    internal class RunsResponseInternal : BaseDataResponse<RunData, RunRange, RunsResponse, Run>
    {
    }
}
