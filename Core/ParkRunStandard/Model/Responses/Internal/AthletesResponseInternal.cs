﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ParkRunPortable.Model.Responses
{
    internal class AthleteData : IData<Athlete>
    {
        [JsonProperty("Athletes")]
        public List<Athlete> Data { get; set; }
    }

    public class AthleteDataRange : IRangeData
    {
        [JsonProperty("AthletesRange")]
        public Range[] Range { get; set; }
    }

    internal class AthletesResponseInternal : BaseDataResponse<AthleteData, AthleteDataRange, AthletesResponse, Athlete>
    {
    }
}
