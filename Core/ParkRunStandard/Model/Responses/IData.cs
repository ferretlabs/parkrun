using System.Collections.Generic;

namespace ParkRunPortable.Model.Responses
{
    internal interface IData<T>
    {
        List<T> Data { get; set; }
    }
}