﻿using System.Collections.Generic;

namespace ParkRunPortable.Model.Responses
{
    public class AthletesResponse : BaseResponse<Athlete>
    {
        public AthletesResponse(List<Athlete> items, int maxItems) 
            : base(items, maxItems)
        {
        }
    }
}