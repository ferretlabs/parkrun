using System;
using System.Diagnostics;
using Newtonsoft.Json;
using ParkRunPortable.Converters;

namespace ParkRunPortable.Model
{
    [DebuggerDisplay("Athlete: {AthleteId}, Position: {FinishPosition}")]
    public class Result
    {
        [JsonProperty("SeriesID")]
        public int SeriesId { get; set; }

        [JsonProperty("EventNumber")]
        public int EventNumber { get; set; }

        [JsonProperty("RunId")]
        public int RunId { get; set; }

        [JsonProperty("EventLongName")]
        public string EventLongName { get; set; }

        [JsonProperty("FinishPosition")]
        public int FinishPosition { get; set; }

        [JsonProperty("GenderPosition")]
        public int GenderPosition { get; set; }

        [JsonProperty("EventDate")]
        public DateTimeOffset EventDate { get; set; }

        [JsonProperty("AthleteID")]
        public int AthleteId { get; set; }

        [JsonProperty("RunTime")]
        public TimeSpan RunTime { get; set; }

        [JsonProperty("PB")]
        [JsonConverter(typeof(BooleanJsonConverter))]
        public bool PersonalBest { get; set; }

        [JsonProperty("Points")]
        public int Points { get; set; }

        [JsonProperty("AgeGrading")]
        public string AgeGrading { get; set; }

        [JsonProperty("AgeCategory")]
        public string AgeCategory { get; set; }

        [JsonProperty("FirstTimer")]
        [JsonConverter(typeof(BooleanJsonConverter))]
        public bool FirstTimer { get; set; }

        [JsonProperty("GenuinePB")]
        [JsonConverter(typeof(BooleanJsonConverter))]
        public bool GenuinePersonalBest { get; set; }

        [JsonProperty("Updated")]
        public DateTimeOffset Updated { get; set; }

        [JsonProperty("HandicapRunTime")]
        public TimeSpan? HandicapRunTime { get; set; }

        [JsonProperty("Assisted")]
        public object Assisted { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("ClubID")]
        public int ClubId { get; set; }

        [JsonProperty("ClubName")]
        public string ClubName { get; set; }

        [JsonProperty("OrganisationID")]
        public int OrganisationId { get; set; }

        [JsonProperty("OrgSubTypeID")]
        public string OrgSubTypeId { get; set; }

        [JsonProperty("HomeRunName")]
        public string HomeRunName { get; set; }

        [JsonProperty("TotalRuns")]
        public int TotalRuns { get; set; }

        [JsonProperty("parkrunClub")]
        public string ParkrunClub { get; set; }
    }
}