﻿using System;
using Newtonsoft.Json;

namespace ParkRunPortable.Model
{
    public class NewsItem
    {
        [JsonProperty("EventNumber")]
        public int EventNumber { get; set; }

        [JsonProperty("ID")]
        public int Id { get; set; }

        [JsonProperty("post_title")]
        public string PostTitle { get; set; }

        [JsonProperty("post_content")]
        public string PostContent { get; set; }

        [JsonProperty("post_date")]
        public DateTimeOffset PostDate { get; set; }

        [JsonProperty("author")]
        public string Author { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("NumberOfComments")]
        public int NumberOfComments { get; set; }
    }
}