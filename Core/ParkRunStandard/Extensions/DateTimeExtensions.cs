﻿using System;

namespace ParkRunPortable.Extensions
{
    internal static class DateTimeExtensions
    {
        internal static string ToParkRunDate(this DateTimeOffset date)
        {
            return date.ToString("yyyyMMdd");
        }
    }
}