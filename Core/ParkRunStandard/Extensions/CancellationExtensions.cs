﻿using System.Threading;
using ParkRunPortable.Model;

namespace ParkRunPortable.Extensions
{
    internal static class CancellationExtensions
    {
        internal static void ThrowParkRunExceptionIfCancelled(this CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                throw new ParkRunException();
            }
        }
    }
}
