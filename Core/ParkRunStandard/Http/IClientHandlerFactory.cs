﻿namespace ParkRunPortable.Http
{
    public interface IClientHandlerFactory
    {
        IClientHandler CreateHandler();
    }
}
