﻿namespace ParkRunPortable.Http
{
    public interface IClientHandler
    {
        bool IsDisposed { get; }
    }
}