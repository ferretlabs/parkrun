﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ParkRunPortable.Converters
{
    internal class ListConverter<T> : JsonConverter
    {
        public override bool CanWrite => false;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var json = reader.Value.ToString();
            var item = JsonConvert.DeserializeObject<T>(json);

            return new List<T> { item };
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(List<T>);
        }
    }
}