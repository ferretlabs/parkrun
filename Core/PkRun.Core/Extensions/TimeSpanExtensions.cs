﻿using System;
using System.Text;

namespace PkRun.Core.Extensions
{
    public static class TimeSpanExtensions
    {
        public static string ToRunTime(this TimeSpan ts)
        {
            var sb = new StringBuilder();
            var hours = Math.Floor(ts.TotalHours);

            if (hours > 0)
            {
                sb.Append(hours);
                sb.Append(":");
            }

            sb.Append(ts.Minutes.ToString("d2"));
            sb.Append(":");
            sb.Append(ts.Seconds.ToString("d2"));

            return sb.ToString();
        }
    }
}
