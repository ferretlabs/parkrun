﻿using System;

namespace PkRun.Core.Extensions
{
    public static class UriExtensions
    {
        public static string ToHttps(this Uri url)
        {
            if (url == null)
            {
                return string.Empty;
            }

            var uri = new UriBuilder("https", url.Host)
            {
                Path = url.AbsolutePath
            };

            if (!string.IsNullOrEmpty(url.Query))
            {
                uri.Query = url.Query.Substring(1);
            }

            return uri.ToString();
        }
    }
}
