﻿using Cimbalino.Toolkit.Services;
using Newtonsoft.Json;

namespace PkRun.Core.Extensions
{
    internal static class ApplicationSettingsHandlerExtensions
    {
        internal static void SerialiseAndSave<T>(this IApplicationSettingsServiceHandler handler, string key, T value)
        {
            if (value != null)
            {
                var json = JsonConvert.SerializeObject(value);
                handler.Set(key, json);
            }
        }

        internal static T GetAndDeserialise<T>(this IApplicationSettingsServiceHandler handler, string key)
        {
            var json = handler.Get<string>(key);
            return string.IsNullOrEmpty(json) ? default(T) : JsonConvert.DeserializeObject<T>(json);
        }
    }
}
