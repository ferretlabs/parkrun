﻿using System;
using PkRun.Core.Services;

namespace PkRun.Core.Extensions
{
    public static class DateTimeOffsetExtensions
    {
        public static string Age(this DateTimeOffset? athleteDateOfBirth, IEnvironmentService environmentService)
        {
            if (!athleteDateOfBirth.HasValue)
            {
                return string.Empty;
            }

            var now = environmentService.Now;
            var birthDate = athleteDateOfBirth.Value;

            var age = now.Year - birthDate.Year;

            if (now.Month < birthDate.Month || (now.Month == birthDate.Month && now.Day < birthDate.Day))
                age--;

            return age.ToString();
        }
    }
}