﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using MetroLog;
using MetroLog.Layouts;
using MetroLog.Targets;
using ParkRunPortable.Logging;
using ILogger = ParkRunPortable.Logging.ILogger;

namespace PkRun.Core.Logging
{
    public class PkRunLogger : ILogger
    {
        private readonly IStorageService _storageService;
        private readonly MetroLog.ILogger _logger;
        private readonly ILogManager _manager;

        public PkRunLogger(IStorageService storageService)
        {
            _storageService = storageService;
#if DEBUG
            LogManagerFactory.DefaultConfiguration.AddTarget(LogLevel.Debug, LogLevel.Fatal, new DebugTarget());
#endif
            LogManagerFactory.DefaultConfiguration.AddTarget(LogLevel.Trace, LogLevel.Fatal, new PkRunStreamingFileTarget(storageService));
            _manager = LogManagerFactory.CreateLogManager();
            _logger = _manager.GetLogger("PkRun");
        }
        
        public void Info(string message, params object[] paramList)
        {
            _logger.Info(message, paramList);
        }

        public void Error(string message, params object[] paramList)
        {
            _logger.Error(message, paramList);
        }

        public void Warn(string message, params object[] paramList)
        {
            _logger.Warn(message, paramList);
        }

        public void Debug(string message, params object[] paramList)
        {
#if DEBUG
            _logger.Debug(message, paramList);
#endif
        }

        public void Fatal(string message, params object[] paramList)
        {
            _logger.Fatal(message, paramList);
        }

        public void FatalException(string message, Exception exception, params object[] paramList)
        {
            _logger.Fatal(message: string.Format(message, paramList), ex: exception);
        }

        public void Log(LogSeverity severity, string message, params object[] paramList)
        {
            switch (severity)
            {
                case LogSeverity.Debug:
                    Debug(message, paramList);
                    break;
                case LogSeverity.Error:
                    Error(message, paramList);
                    break;
                case LogSeverity.Fatal:
                    Fatal(message, paramList);
                    break;
                case LogSeverity.Info:
                    Info(message, paramList);
                    break;
                case LogSeverity.Warn:
                    Warn(message, paramList);
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        public void ErrorException(string message, Exception exception, params object[] paramList)
        {
            _logger.Error(message: string.Format(message, paramList), ex: exception);
        }

        public void LogMultiline(string message, LogSeverity severity, StringBuilder additionalContent)
        {
        }

        public async Task PurgeLogFile()
        {
            if (await _storageService.Local.FileExistsAsync(PkRunStreamingFileTarget.PkRunLogFile))
            {
                await _storageService.Local.DeleteFileAsync(PkRunStreamingFileTarget.PkRunLogFile);
            }
        }

        public Task<Stream> OpenLogFile()
        {
            return _manager.GetCompressedLogs();
        }
    }

    public class PkRunStreamingFileTarget : StreamingFileTarget
    {
        public const string PkRunLogFile = "PkRun.log";

        private readonly IStorageService _storageService;

        public PkRunStreamingFileTarget(IStorageService storageService)
        {
            _storageService = storageService;
        }

        protected override Task WriteTextToFileCore(StreamWriter file, string contents)
        {
            //return _storageService.Local.AppendAllLines(PkRunLogFile, new List<string>{ contents });
            return base.WriteTextToFileCore(file, contents);
        }
    }

    public class DebugTarget : SyncTarget
    {
        public DebugTarget()
            : this(new SingleLineLayout())
        {
        }

        public DebugTarget(Layout layout)
            : base(layout)
        {
        }

        protected override void Write(LogWriteContext context, LogEventInfo entry)
        {
            var message = Layout.GetFormattedString(context, entry);

            Debug.WriteLine(message);
        }
    }
}