﻿using System;

namespace PkRun.Core.Services
{
    public interface IEnvironmentService
    {
        DateTimeOffset Now { get; }
    }

    public class EnvironmentService : IEnvironmentService
    {
        public DateTimeOffset Now => DateTimeOffset.Now;
    }
}
