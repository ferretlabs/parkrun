using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ParkRunPortable.Model;

namespace PkRun.Core.Services
{
    public interface INewsApiHandler
    {
        Task<List<NewsItem>> GetLatestNewsItems(int eventId, CancellationToken token);
        Task<List<NewsItem>> GetNewsItems(int eventId, int offset, CancellationToken token);
        Task<List<NewsItem>> GetCachedItems(int eventId);
        Task<NewsItem> GetNewsItem(int eventId, int newsId, CancellationToken token);
        Task ClearCache(int eventId);
    }
}