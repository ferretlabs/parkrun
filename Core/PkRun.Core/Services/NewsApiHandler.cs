﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using Newtonsoft.Json;
using ParkRunPortable;
using ParkRunPortable.Model;
using ParkRunPortable.Model.Requests;

namespace PkRun.Core.Services
{
    public class NewsApiHandler : INewsApiHandler
    {
        public const string EventCountKey = "Event_News_{0}_Count";
        public const string EventCacheFile = "Event_News_{0}_Cache.json";

        private readonly IParkRunClient _parkRunClient;
        private readonly IApplicationSettingsServiceHandler _settingsService;
        private readonly IStorageServiceHandler _localStorage;

        private readonly SemaphoreSlim _lock = new SemaphoreSlim(1);

        public NewsApiHandler(
            IParkRunClient parkRunClient,
            IApplicationSettingsServiceHandler settingsService,
            IStorageService storageService)
        {
            _parkRunClient = parkRunClient;
            _settingsService = settingsService;
            _localStorage = storageService.Local;
        }

        public async Task<List<NewsItem>> GetLatestNewsItems(int eventId, CancellationToken token)
        {
            var count = GetEventCount(eventId);

            var response = await _parkRunClient.GetNewsForEventAsync(eventId, new FilterOptions{Offset = count}, cancellationToken: token);

            if (response.Items.Any())
            {
                SetEventCount(eventId, response.MaxItems);
                await UpdateCache(eventId, response.Items);
            }

            return response.Items;
        }

        public async Task<List<NewsItem>> GetNewsItems(int eventId, int offset, CancellationToken token)
        {
            var response = await _parkRunClient.GetNewsForEventAsync(eventId, new FilterOptions { Offset = offset }, cancellationToken: token);
            await CreateCacheIfDoesntExist(eventId, response.Items);
            return response.Items;
        }
        
        public Task<NewsItem> GetNewsItem(int eventId, int newsId, CancellationToken token)
        {
            return _parkRunClient.GetNewsItemAsync(eventId, newsId, token);
        }

        public async Task<List<NewsItem>> GetCachedItems(int eventId)
        {
            var fileName = GetFileName(eventId);
            var list = new List<NewsItem>();
            if (await _localStorage.FileExistsAsync(fileName))
            {
                var json = await _localStorage.ReadAllTextAsync(fileName);
                var cachedItems = JsonConvert.DeserializeObject<List<NewsItem>>(json);
                list = cachedItems.OrderBy(x => x.Id).Take(10).ToList();
            }

            return list;
        }

        public async Task ClearCache(int eventId)
        {
            var fileName = GetFileName(eventId);
            if (await _localStorage.FileExistsAsync(fileName))
            {
                await _localStorage.DeleteFileAsync(fileName);
            }

            var key = GetKey(eventId);
            _settingsService.Remove(key);
        }

        private static string GetKey(int eventId)
        {
            return string.Format(EventCountKey, eventId);
        }

        private static string GetFileName(int eventId)
        {
            return string.Format(EventCacheFile, eventId);
        }

        private int GetEventCount(int eventId)
        {
            var key = GetKey(eventId);
            var count = _settingsService.Get(key, 0);
            return count;
        }

        private void SetEventCount(int eventId, int count)
        {
            var key = GetKey(eventId);
            _settingsService.Set(key, count);
        }

        private async Task UpdateCache(int eventId, List<NewsItem> items)
        {
            await _lock.WaitAsync();

            var fileName = GetFileName(eventId);
            var allItems = await GetCachedItems(eventId);

            allItems.AddRange(items);
            allItems = allItems.OrderBy(x => x.Id).ToList();

            var cacheJson = JsonConvert.SerializeObject(allItems);
            await _localStorage.WriteAllTextAsync(fileName, cacheJson);

            _lock.Release();
        }

        private async Task CreateCacheIfDoesntExist(int eventId, List<NewsItem> items)
        {
            var fileName = GetFileName(eventId);
            if (!await _localStorage.FileExistsAsync(fileName))
            {
                await UpdateCache(eventId, items);
            }
        }
    }
}
