﻿using System.Threading;

namespace PkRun.Core.Services
{
    public interface ICancellationManager
    {
        CancellationToken CurrentCancellationToken { get; }

        void CancelPendingOperations();
    }
}