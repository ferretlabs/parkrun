﻿namespace PkRun.Core.Services
{
    public interface IPlatformInformationService
    {
        Platform Platform { get; }
    }

    public enum Platform
    {
        Unknown,
        AndroidMobile,
        AndroidTablet,
        AppleMobile,
        AppleTablet,
        WindowsMobile,
        WindowsDesktop
    }
}
