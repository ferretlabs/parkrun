﻿namespace PkRun.Core.Services
{
    public interface IAnalyticsService
    {
        void Track(string message);
    }

    public class NullAnalyticsService : IAnalyticsService
    {
        public void Track(string message)
        {
        }
    }
}
