﻿using System.Threading;
using System.Threading.Tasks;

namespace PkRun.Core.Services
{
    public class CancellationManager : ICancellationManager
    {
        private readonly object _cancellationTokenSourceLock = new object();
        private CancellationTokenSource _cancellationTokenSource;

        public CancellationToken CurrentCancellationToken
        {
            get
            {
                lock (_cancellationTokenSourceLock)
                {
                    return _cancellationTokenSource.Token;
                }
            }
        }

        public CancellationManager()
        {
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public async void CancelPendingOperations()
        {
            CancellationTokenSource cancellationTokenSource;

            lock (_cancellationTokenSourceLock)
            {
                cancellationTokenSource = _cancellationTokenSource;

                _cancellationTokenSource = new CancellationTokenSource();
            }

            cancellationTokenSource.Cancel();

            // wait for 2.5s for any pensding operation to cancel; this avoids any possible ObjectDisposedException being raised
            await Task.Delay(2500, CancellationToken.None);

            cancellationTokenSource.Dispose();
        }
    }
}