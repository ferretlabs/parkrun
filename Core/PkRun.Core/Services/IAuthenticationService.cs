﻿using System;
using System.Threading.Tasks;
using ParkRunPortable.Model;

namespace PkRun.Core.Services
{
    public interface IAuthenticationService
    {
        event EventHandler<AuthenticationChangedArgs> AuthenticationChanged;
        bool IsSignedIn { get; }
        Athlete SignedInAthlete { get; }
        Task SignIn(string username, string password);
        void SignOut();
    }
}
