﻿using System.Threading.Tasks;

namespace PkRun.Core.Services
{
    public interface INavigationService : Cimbalino.Toolkit.Services.INavigationService
    {
        Task NavigateToHome(object parameters = null);
        Task NavigateToLogin(object parameters = null);
        Task NavigateToNews(object parameters = null);
        Task NavigateToNewsItem(object parameters = null);
        Task NavigateToAthlete(object parameters = null);
        Task NavigateToSettings(object parameters = null);
        Task NavigateToEvent(object parameters = null);
        Task NavigateToRunResults(object parameters = null);
    }
}
