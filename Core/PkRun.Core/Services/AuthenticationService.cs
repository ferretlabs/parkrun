﻿using System;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using ParkRunPortable;
using ParkRunPortable.Model;
using PkRun.Core.Extensions;

namespace PkRun.Core.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        internal const string SignedInAthleteKey = "SignedInAthlete";
        internal const string SignedInTokenKey = "SignedInToken";

        private readonly IApplicationSettingsServiceHandler _settingsService;
        private readonly IParkRunClient _parkRunClient;

        public AuthenticationService(
            IApplicationSettingsServiceHandler settingsService,
            IParkRunClient parkRunClient)
        {
            _settingsService = settingsService;
            _parkRunClient = parkRunClient;

            LoadAthlete();
            LoadToken();

            _parkRunClient.TokenUpdated += ParkRunClientOnTokenUpdated;
        }

        private void ParkRunClientOnTokenUpdated(object sender, Token token)
        {
            if (token != null)
            {
                SetToken(token);
            }
        }

        public bool IsSignedIn => SignedInAthlete != null;
        public Athlete SignedInAthlete { get; private set; }
        public async Task SignIn(string username, string password)
        {
            try
            {
                var token = await _parkRunClient.AuthenticateAsync(username, password);
                SetToken(token);

                var user = await _parkRunClient.GetAthleteProfileAsync(username);
                SetAthlete(user);

                AuthenticationChanged?.Invoke(this, new AuthenticationChangedArgs(true));
            }
            catch (ParkRunException)
            {
                // TODO: Do some logging here I guess
                var s = 1;
                throw;
            }
        }

        public void SignOut()
        {
            _settingsService.Remove(SignedInAthleteKey);
            _settingsService.Remove(SignedInTokenKey);
            SignedInAthlete = null;
            _parkRunClient.SetToken(null);

            AuthenticationChanged?.Invoke(this, new AuthenticationChangedArgs(false));
        }

        private void SetToken(Token token)
        {
            _settingsService.SerialiseAndSave(SignedInTokenKey, token);
            if (token != null)
            {
                _parkRunClient.SetToken(token);
            }
        }

        private void SetAthlete(Athlete athlete)
        {
            SignedInAthlete = athlete;
            _settingsService.SerialiseAndSave(SignedInAthleteKey, athlete);
        }

        private void LoadAthlete()
        {
            var athlete = _settingsService.GetAndDeserialise<Athlete>(SignedInAthleteKey);
            SetAthlete(athlete);
        }

        private void LoadToken()
        {
            var token = _settingsService.GetAndDeserialise<Token>(SignedInTokenKey);
            SetToken(token);
        }

        public event EventHandler<AuthenticationChangedArgs> AuthenticationChanged;
    }
}
