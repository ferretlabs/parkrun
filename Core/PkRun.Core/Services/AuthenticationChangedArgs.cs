﻿using System;

namespace PkRun.Core.Services
{
    public class AuthenticationChangedArgs : EventArgs
    {
        public bool IsSignedIn { get; }

        public AuthenticationChangedArgs(bool isSignedIn)
        {
            IsSignedIn = isSignedIn;
        }
    }
}