﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ParkRunPortable.Model.Requests;
using Xunit;

namespace ParkRunPortableTests
{
    public class ParkRunClientAthleteTests : BaseTests
    {
        private const string ScottsUserId = "A3222517";
        private const int ScottsAthleteId = 3222517;
        private const int MelsAthleteId = 2744074;

        [Fact]
        public async Task GetsCorrectUserProfileByUserName()
        {
            var athlete = await Client.GetAthleteProfileAsync(ScottsUserId, true);

            Assert.NotNull(athlete);
            Assert.Equal("LOVEGROVE", athlete.LastName);
            Assert.Equal("Scott", athlete.FirstName);
        }

        [Fact]
        public async Task GetsCorrectUserProfileByAthleteId()
        {
            var athlete = await Client.GetAthleteProfileAsync(ScottsAthleteId, true);

            Assert.NotNull(athlete);
            Assert.Equal("LOVEGROVE", athlete.LastName);
            Assert.Equal("Scott", athlete.FirstName);
        }

        [Fact]
        public async Task SearchForScottFindsResults()
        {
            var results = await Client.SearchUsersAsync("Scott", "Lovegrove");

            Assert.NotNull(results);
            Assert.True(results.Items.Any());
            Assert.Equal(ScottsAthleteId, results.Items.First().AthleteId);
        }

        [Fact]
        public async Task MelHasDoneHowManyRuns()
        {
            var result = await Client.GetAthleteRunCountAsync(MelsAthleteId);

            Assert.True(result > 1);
        }

        [Fact]
        public async Task MelsRuns()
        {
            var result = await Client.GetRunsForAthleteAsync(MelsAthleteId);

            Assert.NotNull(result);
            Assert.True(result.Items.Count > 1);
        }

        [Fact]
        public async Task MelsRunsAfterSeventeethFeb()
        {
            var result = await Client.GetRunsForAthleteAsync(MelsAthleteId, new FilterOptions
            {
                Limit = 1,
                AfterDate = new DateTimeOffset(new DateTime(2017, 2, 17)),
                BeforeDate = new DateTimeOffset(new DateTime(2017, 2, 24))
            });

            Assert.NotNull(result);
            Assert.True(result.Items.Count == 1);
            Assert.Equal(new DateTime(2017, 2, 18), result.Items.First().EventDate.Date);
        }

        //[Fact]
        //public async Task FollowMel()
        //{
        //    await Client.SendFollowRequestAsync(MelsAthleteId);
        //}
    }
}