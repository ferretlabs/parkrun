﻿using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ParkRunPortableTests
{
    public class ClubTests : BaseTests
    {
        private const int UkCountryCode = 97;

        [Fact]
        public async Task GetClubsForUk()
        {
            var result = await Client.GetClubsByCountryAsync(UkCountryCode);

            Assert.NotNull(result);
            Assert.True(result.Items.Any());
        }
    }
}
