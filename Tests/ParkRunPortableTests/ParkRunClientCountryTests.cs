﻿using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ParkRunPortableTests
{
    public class ParkRunClientCountryTests : BaseTests
    {
        [Fact]
        public async Task GetCountriesList()
        {
            var response = await Client.GetCountriesAsync();

            Assert.NotNull(response);
            Assert.True(response.Items.Any());
        }
    }
}
