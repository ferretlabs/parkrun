﻿using System.Linq;
using System.Threading.Tasks;
using ParkRunPortable.Model.Requests;
using Xunit;

namespace ParkRunPortableTests
{
    public class ParkRunClientEventTests : BaseTests
    {
        private const int PooleParkRunId = 233;

        [Fact]
        public async Task GetPooleParkRun()
        {
            var result = await Client.GetEventAsync(PooleParkRunId);

            Assert.NotNull(result);
            Assert.Equal(PooleParkRunId, result.EventNumber);
        }

        [Fact]
        public async Task GetPooleParkRunCourseData()
        {
            var result = await Client.GetCourseDataAsync(PooleParkRunId);

            Assert.NotNull(result);
        }

        [Fact]
        public async Task GetPooleParkRunRuns()
        {
            var result = await Client.GetRunsForEventAsync(PooleParkRunId);

            Assert.NotNull(result);
        }

        [Fact]
        public async Task GetPooleParkRunResults()
        {
            var runs = await Client.GetRunsForEventAsync(PooleParkRunId, new FilterOptions{Limit = 1});
            var run = runs.Items.FirstOrDefault();
            var results = await Client.GetResultsForRunAsync(PooleParkRunId, run.RunId, run.EventDate);

            Assert.NotNull(results);
        }

        [Fact]
        public async Task GetPooleParkRunNewsFeed()
        {
            var newsFeed = await Client.GetNewsForEventAsync(PooleParkRunId);

            Assert.NotNull(newsFeed);
        }

        [Fact]
        public async Task GetNewsFeedItem()
        {
            var newsFeed = await Client.GetNewsForEventAsync(PooleParkRunId, new FilterOptions { Limit = 1 });
            var feedItem = await Client.GetNewsItemAsync(PooleParkRunId, newsFeed.Items.FirstOrDefault().Id);

            Assert.NotNull(feedItem);
            Assert.NotNull(feedItem.PostContent);
        }
    }
}