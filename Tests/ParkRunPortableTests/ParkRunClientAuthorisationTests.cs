﻿using System.Threading.Tasks;
using ParkRunPortable;
using ParkRunPortable.Model;
using Xunit;

namespace ParkRunPortableTests
{
    public class ParkRunClientAuthorisationTests
    {
        private readonly ParkRunClient _client;

        public ParkRunClientAuthorisationTests()
        {
            _client = new ParkRunClient();
        }

        [Fact]
        public async Task SuccessfullLoginReturnsAuthorisation()
        {
            var response = await _client.AuthenticateAsync("A3222517", "e4Y$82EJw@MZ");
            Assert.NotNull(response);
            Assert.NotNull(_client.Token);
        }

        [Fact]
        public async Task UnsuccessfullLoginCredentialFailure()
        {
            var ex = await Assert.ThrowsAsync<ParkRunException>(async () => await _client.AuthenticateAsync("something", "whatever"));
            Assert.Equal("Invalid username and password combination", ex.Message);
        }

        [Fact]
        public async Task RequestRefreshToken()
        {
            var auth = await _client.RefreshTokenAsync("2226d34d9e99d5dda69b3c1644d55b3a6637cf2d");
            Assert.NotNull(auth);
        }

        [Fact]
        public void WhenSettingTokenDontSetIfDetailsAreTheSame()
        {
            var token = new Token {AccessToken = "MYACCESSTOKEN"};
            _client.SetToken(token);

            var raised = false;

            _client.TokenUpdated += (sender, args) =>
            {
                raised = true;
            };

            _client.SetToken(token);

            Assert.False(raised);
        }
    }
}
