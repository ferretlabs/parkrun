﻿using System;
using ParkRunPortable;
using ParkRunPortable.Model;

namespace ParkRunPortableTests
{
    public class BaseTests
    {
        protected readonly ParkRunClient Client;
        protected BaseTests()
        {
            Client = new ParkRunClient();
            Client.SetToken(new Token
            {
                AccessToken = "0197ad1b71c5bfdf571c991d7f179f65a22a051a",
                RefreshToken = "2226d34d9e99d5dda69b3c1644d55b3a6637cf2d",
                ExpiresAt = DateTimeOffset.Now.AddHours(-1)
            });
        }
    }
}