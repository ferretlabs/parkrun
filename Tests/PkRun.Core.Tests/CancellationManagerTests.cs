﻿using System;
using System.Threading.Tasks;
using PkRun.Core.Services;
using Xunit;

namespace PkRun.Core.Tests
{
    public class CancellationManagerTests : IDisposable
    {
        private readonly CancellationManager _subject = new CancellationManager();

        [Fact]
        public void CancellationManagerContainsCancellationToken()
        {
            Assert.NotNull(_subject.CurrentCancellationToken);
        }

        [Fact]
        public void CancelPendingOperationsCreatesNewCancellationToken()
        {
            var token = _subject.CurrentCancellationToken;

            _subject.CancelPendingOperations();

            Assert.NotEqual(token, _subject.CurrentCancellationToken);
        }

        [Fact]
        public void CancelPendingOperationsInvokesCancellationTokenCancel()
        {
            var task = Task.Delay(2000, _subject.CurrentCancellationToken);

            _subject.CancelPendingOperations();

            Assert.True(task.IsCanceled);
        }

        public void Dispose()
        {
        }
    }
}