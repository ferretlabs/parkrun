﻿using System.Threading;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using Moq;
using Newtonsoft.Json;
using ParkRunPortable;
using ParkRunPortable.Model;
using PkRun.Core.Services;
using Xunit;

namespace PkRun.Core.Tests
{
    public class AuthenticationServiceTests
    {
        private readonly Mock<IApplicationSettingsServiceHandler> _settingsMock = new Mock<IApplicationSettingsServiceHandler>();
        private readonly Mock<IParkRunClient> _parkrunMock = new Mock<IParkRunClient>();

        private AuthenticationService _subject;

        public AuthenticationServiceTests()
        {
            _subject = new AuthenticationService(_settingsMock.Object, _parkrunMock.Object);
        }

        [Fact]
        public void AthleteIsSetIfOneIsSaved()
        {
            var athlete = CreateAthlete();

            _settingsMock.Setup(x => x.Get<string>(AuthenticationService.SignedInAthleteKey))
                .Returns(JsonConvert.SerializeObject(athlete));

            _subject = new AuthenticationService(_settingsMock.Object, _parkrunMock.Object);

            Assert.True(_subject.IsSignedIn);
            Assert.NotNull(_subject.SignedInAthlete);
            Assert.Equal(athlete.FirstName, _subject.SignedInAthlete.FirstName);
            Assert.Equal(athlete.AthleteId, _subject.SignedInAthlete.AthleteId);
        }

        [Fact]
        public void TokenIsSetIfOneIsSaved()
        {
            var token = CreateToken();

            _settingsMock.Setup(x => x.Get<string>(AuthenticationService.SignedInTokenKey))
                .Returns(JsonConvert.SerializeObject(token));

            _subject = new AuthenticationService(_settingsMock.Object, _parkrunMock.Object);

            _parkrunMock.Verify(x => x.SetToken(It.IsAny<Token>()), Times.Once);
        }

        [Fact]
        public async Task SignInSuccessful()
        {
            var token = CreateToken();
            var athlete = CreateAthlete();
            _parkrunMock.Setup(x => x.AuthenticateAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(token);
            _parkrunMock.Setup(x => x.GetAthleteProfileAsync(It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(athlete);

            await _subject.SignIn(It.IsAny<string>(), It.IsAny<string>());

            _settingsMock.Verify(x => x.Set(AuthenticationService.SignedInTokenKey, It.IsAny<string>()), Times.Once);
            _settingsMock.Verify(x => x.Set(AuthenticationService.SignedInAthleteKey, It.IsAny<string>()), Times.Once);

            Assert.True(_subject.IsSignedIn);
            Assert.NotNull(_subject.SignedInAthlete);
            Assert.Equal(athlete.FirstName, _subject.SignedInAthlete.FirstName);
            Assert.Equal(athlete.AthleteId, _subject.SignedInAthlete.AthleteId);
        }

        [Fact]
        public async Task SignInUnsuccessful()
        {
            _parkrunMock.Setup(x => x.AuthenticateAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .Throws<ParkRunException>();

            await Assert.ThrowsAsync<ParkRunException>(async () => await _subject.SignIn(It.IsAny<string>(), It.IsAny<string>()));

            _settingsMock.Verify(x => x.Set(AuthenticationService.SignedInTokenKey, It.IsAny<string>()), Times.Never);
            _settingsMock.Verify(x => x.Set(AuthenticationService.SignedInAthleteKey, It.IsAny<string>()), Times.Never);

            Assert.False(_subject.IsSignedIn);
            Assert.Null(_subject.SignedInAthlete);
        }

        [Fact]
        public void SignOut()
        {
            _subject.SignOut();

            _settingsMock.Verify(x => x.Remove(AuthenticationService.SignedInAthleteKey), Times.Once);
            _settingsMock.Verify(x => x.Remove(AuthenticationService.SignedInTokenKey), Times.Once);
            _parkrunMock.Verify(x => x.SetToken(null), Times.Once);
            Assert.Null(_subject.SignedInAthlete);
            Assert.False(_subject.IsSignedIn);
        }

        private static Token CreateToken()
        {
            const string accessToken = "ACCESSTOKEN";
            var token = new Token { AccessToken = accessToken };
            return token;
        }

        private static Athlete CreateAthlete()
        {
            const int athleteId = 123456;
            const string athleteName = "Scott";
            var athlete = new Athlete { AthleteId = athleteId, FirstName = athleteName };
            return athlete;
        }
    }
}
