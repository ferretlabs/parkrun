﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using Moq;
using Newtonsoft.Json;
using ParkRunPortable;
using ParkRunPortable.Model;
using ParkRunPortable.Model.Requests;
using ParkRunPortable.Model.Responses;
using PkRun.Core.Services;
using Xunit;

namespace PkRun.Core.Tests
{
    public class NewsApiHandlerTests
    {
        private const int PooleEventId = 233;
        private const string PooleEventKey = "Event_News_233_Count";
        private const string PooleEventFile = "Event_News_233_Cache.json";

        private readonly Mock<IParkRunClient> _parkRunMock = new Mock<IParkRunClient>();
        private readonly Mock<IApplicationSettingsServiceHandler> _settingsServiceMock = new Mock<IApplicationSettingsServiceHandler>();
        private readonly Mock<IStorageService> _storageServiceMock = new Mock<IStorageService>();
        private readonly Mock<IStorageServiceHandler> _localStorageMock = new Mock<IStorageServiceHandler>();

        private readonly NewsApiHandler _subject;

        public NewsApiHandlerTests()
        {
            _storageServiceMock.SetupGet(x => x.Local).Returns(_localStorageMock.Object);

            _subject = new NewsApiHandler(
                _parkRunMock.Object,
                _settingsServiceMock.Object,
                _storageServiceMock.Object);
        }

        [Fact]
        public async Task SaveNewItemsWhenThereAreNewItemsReturnedAndCachedItems()
        {
            _settingsServiceMock.Setup(x => x.Get(PooleEventKey, 0)).Returns(308);

            IfExistingCacheItems(PooleEventId);

            AndParkRunReturnsNewNewsItems(PooleEventId);

            var response = await _subject.GetLatestNewsItems(PooleEventId, It.IsAny<CancellationToken>());

            Assert.Equal(3, response.Count);
            
            _settingsServiceMock.Verify(x => x.Set(PooleEventKey, 311), Times.Once);
            _localStorageMock.Verify(x => x.WriteAllTextAsync(PooleEventFile, It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task SaveNewItemsWhenThereAreNewItemsReturnedAndNoCachedItems()
        {
            _settingsServiceMock.Setup(x => x.Get(PooleEventKey, 0)).Returns(308);

            IfNoExistingCacheItems();

            AndParkRunReturnsNewNewsItems(PooleEventId);

            var response = await _subject.GetLatestNewsItems(PooleEventId, It.IsAny<CancellationToken>());

            Assert.Equal(3, response.Count);

            _localStorageMock.Verify(x => x.ReadAllTextAsync(PooleEventFile), Times.Never);
            _localStorageMock.Verify(x => x.WriteAllTextAsync(PooleEventFile, It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task DontDoAnythingIfThereAreNoNewItems()
        {
            _settingsServiceMock.Setup(x => x.Get(PooleEventKey, 0)).Returns(308);
            _parkRunMock.Setup(x => x.GetNewsForEventAsync(PooleEventId, It.IsAny<FilterOptions>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new NewsResponse(new List<NewsItem>(), 308));

            var response = await _subject.GetLatestNewsItems(PooleEventId, It.IsAny<CancellationToken>());

            Assert.Equal(0, response.Count);

            _localStorageMock.Verify(x => x.WriteAllTextAsync(PooleEventFile, It.IsAny<string>()), Times.Never);
            _settingsServiceMock.Verify(x => x.Set(PooleEventKey, It.IsAny<int>()), Times.Never);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(10)]
        [InlineData(20)]
        [InlineData(30)]
        public async Task ReturnItemsIfCachedExists(int numberOfCachedItems)
        {
            IfExistingCacheItems(PooleEventId, numberOfCachedItems);

            var items = await _subject.GetCachedItems(PooleEventId);

            Assert.True(items.Count <= 10);
            Assert.True(items.Count > 0);
        }

        [Fact]
        public async Task ReturnsNoItemsIfCacheEmpty()
        {
            IfNoExistingCacheItems();

            var items = await _subject.GetCachedItems(PooleEventId);

            Assert.Equal(0, items.Count);
        }

        [Fact]
        public async Task GetItemsCreatesCacheIfNoCacheExists()
        {
            IfNoExistingCacheItems();

            AndParkRunReturnsNewNewsItems(PooleEventId);

            await _subject.GetNewsItems(PooleEventId, 10, CancellationToken.None);

            _localStorageMock.Verify(x => x.WriteAllTextAsync(PooleEventFile, It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task GetItemsCreatesNoCacheIfOneExists()
        {
            IfExistingCacheItems(PooleEventId);

            AndParkRunReturnsNewNewsItems(PooleEventId);

            await _subject.GetNewsItems(PooleEventId, 10, CancellationToken.None);

            _localStorageMock.Verify(x => x.WriteAllTextAsync(PooleEventFile, It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async Task ClearItemsActuallyClearsItems()
        {
            IfExistingCacheItems(PooleEventId);

            await _subject.ClearCache(PooleEventId);

            _localStorageMock.Verify(x => x.DeleteFileAsync(PooleEventFile), Times.Once);
            _settingsServiceMock.Verify(x => x.Remove(PooleEventKey), Times.Once);
        }

        private void IfExistingCacheItems(int eventId, int numberOfCacheItems = 10)
        {
            var list = new List<NewsItem>();
            var days = numberOfCacheItems * 2 * 7;
            for (var i = 1; i <= numberOfCacheItems; i++)
            {
                list.Add(new NewsItem { Id = i, EventNumber = eventId, PostDate = DateTimeOffset.Now.AddDays(-days) });
                days = days - 7;
            }

            _localStorageMock.Setup(x => x.FileExistsAsync(PooleEventFile)).ReturnsAsync(true);
            _localStorageMock.Setup(x => x.ReadAllTextAsync(PooleEventFile)).ReturnsAsync(JsonConvert.SerializeObject(list));
        }

        private void IfNoExistingCacheItems()
        {
            _localStorageMock.Setup(x => x.FileExistsAsync(PooleEventFile)).ReturnsAsync(false);
        }

        private void AndParkRunReturnsNewNewsItems(int eventId, int numberOfNewItems = 3, int startNewIdsFromNumber = 15)
        {
            var list = new List<NewsItem>();
            var days = numberOfNewItems * 7;
            for (var i = 1; i <= numberOfNewItems; i++)
            {
                list.Add(new NewsItem { Id = startNewIdsFromNumber + i, EventNumber = PooleEventId, PostDate = DateTimeOffset.Now.AddDays(-days) });
                days = days - 7;
            }

            var response = new NewsResponse(list, 311);
            _parkRunMock.Setup(x => x.GetNewsForEventAsync(eventId, It.IsAny<FilterOptions>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(response);
        }
    }
}
