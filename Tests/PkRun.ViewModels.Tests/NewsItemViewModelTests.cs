﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using ParkRunPortable.Model;
using PkRun.Core.Services;
using PkRun.ViewModels.Items;
using Xunit;

namespace PkRun.ViewModels.Test
{
    public class NewsItemViewModelTests : TestBaseViewModel
    {
        private readonly Mock<INewsApiHandler> _newsApiMock = new Mock<INewsApiHandler>();
        private readonly Mock<INavigationService> _navigationMock = new Mock<INavigationService>();
        private readonly Mock<ICancellationManager> _cancellationMock = new Mock<ICancellationManager>();

        private readonly NewsItem _baseNewsItem = new NewsItem
        {
            Author = "Scott Lovegrove",
            EventNumber = 233,
            Id = 308,
            PostTitle = "Some Park Run Info",
            PostDate = new DateTimeOffset(new DateTime(2017, 3, 4))
        };

        private readonly NewsItemViewModel _subject;

        public NewsItemViewModelTests()
        {
            _subject = new NewsItemViewModel(_baseNewsItem, _newsApiMock.Object, _navigationMock.Object, _cancellationMock.Object);
        }

        [Fact]
        public async Task TriggeringNavigationCommandTakesYouToPostDetails()
        {
            await _subject.NavigateToItemCommand.ExecuteAsync(null);

            _navigationMock.Verify(x => x.NavigateToNewsItem(_baseNewsItem), Times.Once);
        }

        [Fact]
        public async Task RequestingDetailsGetsMorePostInformation()
        {
            var updatedItem = _baseNewsItem;
            updatedItem.PostContent = "<p>Some HTML Here</p>";

            _newsApiMock.Setup(x => x.GetNewsItem(_baseNewsItem.EventNumber, _baseNewsItem.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(updatedItem);

            await _subject.LoadPostDetailsCommand.ExecuteAsync(null);

            Assert.Equal("<p>Some HTML Here</p>", _subject.NewsItem.PostContent);
        }
    }
}
