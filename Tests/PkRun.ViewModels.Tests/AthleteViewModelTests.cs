﻿using System.Threading;
using System.Threading.Tasks;
using Moq;
using ParkRunPortable;
using ParkRunPortable.Model;
using PkRun.Core.Services;
using PkRun.ViewModels.Test.Helpers;
using Xunit;

namespace PkRun.ViewModels.Test
{
    public class AthleteViewModelTests : TestBaseViewModel
    {
        private readonly Mock<IParkRunClient> _parkRunMock = new Mock<IParkRunClient>();
        private readonly Mock<INavigationService> _navigaitonMock = new Mock<INavigationService>();
        private readonly Mock<ICancellationManager> _cancellationMock = new Mock<ICancellationManager>();
        private readonly Mock<IAuthenticationService> _authenticationMock = new Mock<IAuthenticationService>();

        private readonly AthleteViewModel _subject;

        public AthleteViewModelTests()
        {
            _subject = new AthleteViewModel(
                _parkRunMock.Object,
                _navigaitonMock.Object,
                _cancellationMock.Object,
                _authenticationMock.Object);
        }

        [Fact]
        public async Task WhenAnIntIsPassedThroughGetReleventAthlete()
        {
            await _subject.OnNavigatedToAsync(TestNavArgs.Forward(123456));

            _parkRunMock.Verify(x => x.GetAthleteProfileAsync(123456, true, It.IsAny<CancellationToken>()), Times.Once);
        }

        [Fact]
        public async Task WhenAnAthleteIsPassedThroughGetExtendedAthleteDetails()
        {
            await _subject.OnNavigatedToAsync(TestNavArgs.Forward(new Athlete {AthleteId = 123456}));

            _parkRunMock.Verify(x => x.GetAthleteProfileAsync(123456, true, It.IsAny<CancellationToken>()), Times.Once);
        }

        [Fact]
        public async Task WhenInavlidParametersPassedThroughNavigateBack()
        {
            await _subject.OnNavigatedToAsync(TestNavArgs.Forward(string.Empty));

            _navigaitonMock.Verify(x => x.GoBack(), Times.Once);
        }
    }
}
