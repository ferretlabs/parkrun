﻿using System;
using Moq;
using ParkRunPortable.Model;
using PkRun.Core.Services;
using PkRun.ViewModels.Items;
using Xunit;

namespace PkRun.ViewModels.Test
{
    public class ResultItemViewModelTests : TestBaseViewModel
    {
        private const int ScottsAthleteId = 3222517;
        private readonly Mock<INavigationService> _navigationMock = new Mock<INavigationService>();
        private readonly Mock<IAuthenticationService> _authenticationMock = new Mock<IAuthenticationService>();

        private readonly Result _result = new Result
        {
            AthleteId = ScottsAthleteId,
            FinishPosition = 1,
            RunTime = TimeSpan.FromSeconds(2100),
            FirstName = "Scott",
            LastName = "LOVEGROVE",
            PersonalBest = false
        };

        private readonly ResultItemViewModel _subject;

        public ResultItemViewModelTests()
        {
            _subject = new ResultItemViewModel(
                _result,
                _navigationMock.Object,
                _authenticationMock.Object);
        }

        [Fact]
        public void PropertiesAreRenderedCorrectly()
        {
            Assert.Equal("Scott Lovegrove", _subject.RunnerName);
            Assert.Equal("1", _subject.FinishPosition);
            Assert.Equal("35:00", _subject.FinishTime);
            Assert.Equal(false, _subject.IsPersonalBest);
        }

        [Fact]
        public void RunTimeOverOneHourDisplayedCorrectly()
        {
            _subject.Result.RunTime = TimeSpan.FromHours(1.2);
            Assert.Equal("1:12:00", _subject.FinishTime);
        }

        [Fact]
        public void IfRunnerAndSignedInUserMatchFlagIsCorrect()
        {
            _authenticationMock.SetupGet(x => x.SignedInAthlete)
                .Returns(new Athlete {AthleteId = ScottsAthleteId});

            Assert.True(_subject.IsSignedInUser);
        }

        [Fact]
        public void IfRunnerIsNotSignedInUserFlagIsCorrect()
        {
            _authenticationMock.SetupGet(x => x.SignedInAthlete)
                .Returns(new Athlete { AthleteId = 123456 });

            Assert.False(_subject.IsSignedInUser);
        }
    }
}
