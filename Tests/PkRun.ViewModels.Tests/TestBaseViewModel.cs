﻿using GalaSoft.MvvmLight.Ioc;
using Moq;
using ParkRunPortable.Logging;
using PkRun.Core.Services;

namespace PkRun.ViewModels.Test
{
    public abstract class TestBaseViewModel
    {
        protected readonly Mock<IEnvironmentService> EnvironmentMock = new Mock<IEnvironmentService>();
        protected TestBaseViewModel()
        {
            if (!SimpleIoc.Default.IsRegistered<ILogger>())
                SimpleIoc.Default.Register<ILogger, NullLogger>();

            if(!SimpleIoc.Default.IsRegistered<IAnalyticsService>())
                SimpleIoc.Default.Register<IAnalyticsService, NullAnalyticsService>();

            if(!SimpleIoc.Default.IsRegistered<IEnvironmentService>())
                SimpleIoc.Default.Register<IEnvironmentService>(() => EnvironmentMock.Object);
        }
    }
}
