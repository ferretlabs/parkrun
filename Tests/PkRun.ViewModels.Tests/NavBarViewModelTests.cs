﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using Moq;
using ParkRunPortable;
using ParkRunPortable.Model;
using PkRun.Core.Services;
using Xunit;
using INavigationService = PkRun.Core.Services.INavigationService;

namespace PkRun.ViewModels.Test
{
    public class NavBarViewModelTests : TestBaseViewModel
    {
        private readonly Mock<INavigationService> _navigationMock = new Mock<INavigationService>();
        private readonly Mock<IAuthenticationService> _authenticationMock = new Mock<IAuthenticationService>();
        private readonly Mock<IParkRunClient> _parkRunMock = new Mock<IParkRunClient>();
        private readonly Mock<IDispatcherService> _dispatcherMock = new Mock<IDispatcherService>();

        private readonly NavBarViewModel _subject;

        private readonly Athlete _scott = new Athlete
        {
            AthleteId = 123456,
            FirstName = "SCOTT",
            LastName = "Lovegrove",
            DateOfBirth = new DateTimeOffset(new DateTime(1982, 7, 12)),
            Avatar = "www.image.com/image.jpg"
        };

        private readonly Athlete _mel = new Athlete
        {
            AthleteId = 654321,
            FirstName = "MELANIE",
            LastName = "Lovegrove",
            DateOfBirth = new DateTimeOffset(new DateTime(1982, 10, 27)),
            Avatar = "www.image.com/image.jpg"
        };

        public NavBarViewModelTests()
        {
            GivenIHaveASignedInAthlete();

            _subject = new NavBarViewModel(
                _navigationMock.Object,
                _authenticationMock.Object,
                _parkRunMock.Object,
                _dispatcherMock.Object);
        }

        [Fact]
        public void SignedInUserIsSetCorrectly()
        {
            Assert.NotNull(_subject.Athlete);
            Assert.Equal("Scott Lovegrove", _subject.Athlete.FullName);
        }

        [Fact]
        public void SigningOutRemovesSignedInUser()
        {
            UserSignedOut();

            Assert.Null(_subject.Athlete);
        }

        [Fact]
        public void SigningInWithAnotherUserIsSetCorrectly()
        {
            UserSignedOut();

            SignedInWithAnotherAthlete();

            Assert.NotNull(_subject.Athlete);
            Assert.Equal("Melanie Lovegrove", _subject.Athlete.FullName);
        }

        [Fact]
        public async Task NavigateCommandGoesToCorrectPlace()
        {
            var item = _subject.MenuItems.First();

            Assert.Equal("Home", item.Title);

            await _subject.NavigateToCommand.ExecuteAsync(item);

            _navigationMock.Verify(x => x.NavigateToHome(null), Times.Once);
        }

        [Fact]
        public void SignOutActuallySignsOut()
        {
            _authenticationMock.SetupGet(x => x.SignedInAthlete).Returns((Athlete)null);
            _authenticationMock.Setup(x => x.SignOut())
                .Raises(x => x.AuthenticationChanged += null, new AuthenticationChangedArgs(false));

            _subject.SignOutCommand.Execute(null);
            
            _authenticationMock.Verify(x => x.SignOut(), Times.Once);

            Assert.Null(_subject.Athlete);
        }

        [Fact]
        public void NavigationToProfileGoesToSignedInUser()
        {
            _subject.NavigateToProfileCommand.Execute(null);

            _navigationMock.Verify(x => x.NavigateToAthlete(_scott), Times.Once);
        }

        private void UserSignedOut()
        {
            _authenticationMock.SetupGet(x => x.SignedInAthlete).Returns((Athlete)null);
            _authenticationMock.Raise(x => x.AuthenticationChanged += null, new AuthenticationChangedArgs(false));
        }

        private void GivenIHaveASignedInAthlete()
        {
            _authenticationMock.SetupGet(x => x.SignedInAthlete)
                .Returns(_scott);
        }

        private void SignedInWithAnotherAthlete()
        {
            _authenticationMock.SetupGet(x => x.SignedInAthlete)
                .Returns(_mel);
            _authenticationMock.Raise(x => x.AuthenticationChanged += null, new AuthenticationChangedArgs(true));
        }
    }
}
