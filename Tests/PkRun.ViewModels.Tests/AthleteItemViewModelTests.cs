﻿using System;
using System.Globalization;
using Moq;
using ParkRunPortable;
using ParkRunPortable.Model;
using PkRun.Core.Services;
using PkRun.ViewModels.Items;
using Xunit;

namespace PkRun.ViewModels.Test
{
    public class AthleteItemViewModelTests : TestBaseViewModel
    {
        private readonly Mock<IParkRunClient> _parkrunMock = new Mock<IParkRunClient>();
        private readonly Mock<IAuthenticationService> _authMock = new Mock<IAuthenticationService>();

        private AthleteItemViewModel _subject;

        private readonly Athlete _athlete = new Athlete
        {
            AthleteId = 123456,
            FirstName = "SCOTT",
            LastName = "Lovegrove",
            DateOfBirth = new DateTimeOffset(new DateTime(1982, 7, 12)),
            Avatar = "www.image.com/image.jpg"
        };

        public AthleteItemViewModelTests()
        {
            _subject = new AthleteItemViewModel(_athlete, _parkrunMock.Object, _authMock.Object);
        }

        [Fact]
        public void EnsureNamesAreRenderedProperly()
        {
            Assert.Equal("Scott", _subject.FirstName);
            Assert.Equal("Lovegrove", _subject.LastName);
            Assert.Equal("Scott Lovegrove", _subject.FullName);
        }

        [Fact]
        public void IfExistsEnsureDateOfBirthRenderedProperly()
        {
            Assert.Equal("12/07/1982", _subject.DateOfBirth);
        }

        [Fact]
        public void IfDateOfBirthNotSetRenderCorrectly()
        {
            _athlete.DateOfBirth = null;
            _subject = new AthleteItemViewModel(_athlete, _parkrunMock.Object, _authMock.Object);

            Assert.Null(_subject.DateOfBirth);
        }

        [Fact]
        public void AthleteIsSignedInAthlete()
        {
            _authMock.SetupGet(x => x.SignedInAthlete).Returns(new Athlete {AthleteId = 123456});

            Assert.True(_subject.IsSignedInUser);
        }

        [Fact]
        public void AthleteIsNotSignedInAthlete()
        {
            _authMock.SetupGet(x => x.SignedInAthlete).Returns(new Athlete { AthleteId = 654321 });

            Assert.False(_subject.IsSignedInUser);
        }

        [Fact]
        public void EnsureAvatarUrlIsFormattedCorrectly()
        {
            Assert.Equal("http://www.image.com/image.jpg", _subject.Avatar);
        }

        [Theory]
        [InlineData("1982-07-12", "34")]
        [InlineData("1982-10-27", "34")]
        [InlineData("1982-03-07", "35")]
        [InlineData("2001-07-12", "15")]
        [InlineData("2001-02-14", "16")]
        [InlineData("1950-06-21", "66")]
        [InlineData("1982-03-19", "35")]
        [InlineData(null, "")]
        public void AgeOfAthleteIsCalculatedCorrectly(string dob, string expectedAge)
        {
            EnvironmentMock.SetupGet(x => x.Now).Returns(new DateTimeOffset(2017, 03, 19, 1, 1, 1, TimeSpan.Zero));

            DateTimeOffset? date = null;
            if (!string.IsNullOrEmpty(dob))
            {
                date = DateTimeOffset.Parse(dob, new CultureInfo("en-GB"));
            }

            _athlete.DateOfBirth = date;
            _subject.UpdateAthlete(_athlete);

            Assert.Equal(expectedAge, _subject.Age);
        }
    }
}