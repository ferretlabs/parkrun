﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using ParkRunPortable;
using ParkRunPortable.Model;
using ParkRunPortable.Model.Requests;
using ParkRunPortable.Model.Responses;
using PkRun.Core.Services;
using PkRun.ViewModels.Test.Helpers;
using Xunit;
using INavigationService = PkRun.Core.Services.INavigationService;

namespace PkRun.ViewModels.Test
{
    public class EventViewModelTests : TestBaseViewModel
    {
        private readonly Mock<IParkRunClient> _parkRunMock = new Mock<IParkRunClient>();
        private readonly Mock<INavigationService> _navigationMock = new Mock<INavigationService>();
        private readonly Mock<ICancellationManager> _cancellationMock = new Mock<ICancellationManager>();

        private readonly EventViewModel _subject;

        public EventViewModelTests()
        {
            _subject = new EventViewModel(
                _parkRunMock.Object,
                _navigationMock.Object,
                _cancellationMock.Object);

            _cancellationMock.SetupGet(x => x.CurrentCancellationToken).Returns(default(CancellationToken));
        }

        [Fact]
        public async Task ValidCourseRouteMapParsedCorrectly()
        {
            _parkRunMock.Setup(x => x.GetCourseRouteMapAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync("<iframe src=\"http://maps.google.com/?something=else&amp;other=things&amp;onemore=thing\"></iframe>");

            await _subject.OnNavigatedToAsync(CreateArgs());

            Assert.Equal("https://maps.google.com/?something=else&other=things&onemore=thing", _subject.MapUrl);
            VerifyLoadDataCallsAllHappened();
        }

        [Fact]
        public async Task ValidCourseRouteMapParsedCorrectlyWithNoUrlParams()
        {
            _parkRunMock.Setup(x => x.GetCourseRouteMapAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync("<iframe src=\"http://maps.google.com/\"></iframe>");

            await _subject.OnNavigatedToAsync(CreateArgs());

            Assert.Equal("https://maps.google.com/", _subject.MapUrl);
            VerifyLoadDataCallsAllHappened();
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("oisdjflisjdoifjsdfsdifj")]
        [InlineData("<iframe src=\"myrubbishaddress\"></iframe>")]
        public async Task InvalidCourseRouteMapShouldGiveNothing(string response)
        {
            _parkRunMock.Setup(x => x.GetCourseRouteMapAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(response);

            await _subject.OnNavigatedToAsync(CreateArgs());

            Assert.Equal(string.Empty, _subject.MapUrl);
            VerifyLoadDataCallsAllHappened();
        }

        [Fact]
        public async Task EventDataReturnedEventNameSet()
        {
            _parkRunMock.Setup(x => x.GetEventAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new Event { EventLongName = "Poole Park run" });

            await _subject.OnNavigatedToAsync(CreateArgs());

            Assert.NotNull(_subject.Event);
            Assert.Equal("Poole Park run", _subject.Name);
            VerifyLoadDataCallsAllHappened();
        }

        [Fact]
        public async Task LatestRunReturned()
        {
            _parkRunMock.Setup(x => x.GetRunsForEventAsync(It.IsAny<int>(), It.IsAny<FilterOptions>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new RunsResponse(new List<Run>
                {
                    new Run
                    {
                        EventDate = new DateTimeOffset(2017, 3, 4, 0, 0,0, TimeSpan.Zero),
                        NumberRunners = 800
                    }
                }, 2));

            await _subject.OnNavigatedToAsync(CreateArgs());

            Assert.NotNull(_subject.LatestRun);
            Assert.Equal("04 March 2017", _subject.LatestRunDate);
            VerifyLoadDataCallsAllHappened();
        }

        [Fact]
        public async Task NoLatestRunsReturned()
        {
            _parkRunMock.Setup(x => x.GetRunsForEventAsync(It.IsAny<int>(), It.IsAny<FilterOptions>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new RunsResponse(new List<Run>(), 10));

            await _subject.OnNavigatedToAsync(CreateArgs());

            Assert.Null(_subject.LatestRun);
            Assert.Null(_subject.LatestRunDate);
            VerifyLoadDataCallsAllHappened();
        }

        [Fact]
        public async Task NoLatestRunsReturnedNullList()
        {
            _parkRunMock.Setup(x => x.GetRunsForEventAsync(It.IsAny<int>(), It.IsAny<FilterOptions>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new RunsResponse(null, 10));

            await _subject.OnNavigatedToAsync(CreateArgs());

            Assert.Null(_subject.LatestRun);
            Assert.Null(_subject.LatestRunDate);
            VerifyLoadDataCallsAllHappened();
        }

        [Fact]
        public async Task LatestRunReturnedDontChooseEmptyRun()
        {
            _parkRunMock.Setup(x => x.GetRunsForEventAsync(It.IsAny<int>(), It.IsAny<FilterOptions>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new RunsResponse(new List<Run>
                {
                    new Run
                    {
                        EventDate = new DateTimeOffset(2017, 3, 11, 0, 0,0, TimeSpan.Zero),
                        NumberRunners = 0
                    },
                    new Run
                    {
                        EventDate = new DateTimeOffset(2017, 3, 4, 0, 0,0, TimeSpan.Zero),
                        NumberRunners = 800
                    }
                }, 2));

            await _subject.OnNavigatedToAsync(CreateArgs());

            Assert.NotNull(_subject.LatestRun);
            Assert.Equal("04 March 2017", _subject.LatestRunDate);
            VerifyLoadDataCallsAllHappened();
        }

        [Fact]
        public async Task ShowCorrectStartTimeIfOneExists()
        {
            _parkRunMock.Setup(x => x.GetCourseDataAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new CourseDataResponse(new List<CourseDataInfo>
                {
                    new CourseDataInfo{Metakey = CourseDataMetaKey.StartTime, Value = "9:00am", EnglishPhrase = "Start time"}
                }, 2));

            await _subject.OnNavigatedToAsync(CreateArgs());

            Assert.NotNull(_subject.CourseDataResponse);
            Assert.Equal("9:00am", _subject.StartTime);
            VerifyLoadDataCallsAllHappened();
        }

        [Fact]
        public async Task ShowNoStartTimeIfNoneExists()
        {
            _parkRunMock.Setup(x => x.GetCourseDataAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new CourseDataResponse(new List<CourseDataInfo>(), 2));

            await _subject.OnNavigatedToAsync(CreateArgs());

            Assert.NotNull(_subject.CourseDataResponse);
            Assert.Equal(null, _subject.StartTime);
            VerifyLoadDataCallsAllHappened();
        }

        [Fact]
        public async Task ShowNoStartTimeIfResponseContainsNoMetaData()
        {
            _parkRunMock.Setup(x => x.GetCourseDataAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new CourseDataResponse(null, 2));

            await _subject.OnNavigatedToAsync(CreateArgs());

            Assert.NotNull(_subject.CourseDataResponse);
            Assert.Equal(null, _subject.StartTime);
            VerifyLoadDataCallsAllHappened();
        }

        [Fact]
        public async Task EventDetailsDoesntContainEmptyItems()
        {
            _parkRunMock.Setup(x => x.GetCourseDataAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new CourseDataResponse(new List<CourseDataInfo>
                {
                    new CourseDataInfo{ Value = string.Empty },
                    new CourseDataInfo{ Value = "Something else" }
                }, 2));

            await _subject.OnNavigatedToAsync(CreateArgs());

            Assert.NotNull(_subject.EventDetails);
            Assert.Equal(1, _subject.EventDetails.Count);
        }

        private static TestNavArgs CreateArgs()
        {
            return TestNavArgs.Forward(new EventNavigationArgs(233));
        }

        private void VerifyLoadDataCallsAllHappened()
        {
            _parkRunMock.Verify(x => x.GetCourseRouteMapAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()), Times.Once);
            _parkRunMock.Verify(x => x.GetEventAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()), Times.Once);
            _parkRunMock.Verify(x => x.GetRunsForEventAsync(It.IsAny<int>(), It.IsAny<FilterOptions>(), It.IsAny<CancellationToken>()), Times.Once);
            _parkRunMock.Verify(x => x.GetCourseDataAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
