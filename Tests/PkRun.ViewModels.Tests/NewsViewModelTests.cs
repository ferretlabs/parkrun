﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using Moq;
using ParkRunPortable.Model;
using PkRun.Core.Services;
using Xunit;
using INavigationService = PkRun.Core.Services.INavigationService;

namespace PkRun.ViewModels.Test
{
    public class NewsViewModelTests : TestBaseViewModel
    {
        private const int PooleParkRunId = 233;
        private readonly Mock<INavigationService> _navigationMock = new Mock<INavigationService>();
        private readonly Mock<INewsApiHandler> _newsHandlerMock = new Mock<INewsApiHandler>();
        private readonly Mock<ICancellationManager> _cancellationMock = new Mock<ICancellationManager>();

        private readonly NewsViewModel _subject;

        public NewsViewModelTests()
        {
            _subject = new NewsViewModel(
                _navigationMock.Object, 
                _newsHandlerMock.Object,
                _cancellationMock.Object);
        }

        [Fact]
        public async Task WhenEventIdGivenCheckItsInTheCollection()
        {
            await GivenIHaveAnEventIdThen();

            Assert.Equal(PooleParkRunId, _subject.NewsItems.EventId);
        }

        [Fact]
        public async Task InitialLoadProvidesItems()
        {
            await GivenIHaveAnEventIdThen();

            AndAnInitialFetchOfNewsHappens();

            await _subject.NewsItems.LoadMoreItemsAsync(0);

            Assert.Equal(10, _subject.NewsItems.Count);
        }

        [Fact]
        public async Task WhenNextLoadOfItemsRequestedTheyGetAdded()
        {
            await GivenIHaveAnEventIdThen();

            AndAnInitialFetchOfNewsHappens();

            await _subject.NewsItems.LoadMoreItemsAsync(0);

            AndAnExtraLoadOfNewsItems();

            await _subject.NewsItems.LoadMoreItemsAsync(0);

            Assert.Equal(20, _subject.NewsItems.Count);
        }

        private Task GivenIHaveAnEventIdThen()
        {
            return _subject.OnNavigatedToAsync(new NavigationServiceNavigationEventArgs(
                NavigationServiceNavigationMode.Forward,
                GetType(),
                PooleParkRunId,
                null));
        }

        private void AndAnInitialFetchOfNewsHappens(int numberOfNewItems = 10)
        {
            var list = new List<NewsItem>();
            var days = numberOfNewItems * 7;
            for (var i = 1; i <= numberOfNewItems; i++)
            {
                list.Add(new NewsItem { Id = i + i, EventNumber = PooleParkRunId, PostDate = DateTimeOffset.Now.AddDays(-days) });
                days = days - 7;
            }

            _newsHandlerMock.Setup(x => x.GetNewsItems(PooleParkRunId, 0, It.IsAny<CancellationToken>()))
                .ReturnsAsync(list);
        }

        private void AndAnExtraLoadOfNewsItems(int numberOfNewItems = 10)
        {
            var list = new List<NewsItem>();
            var days = numberOfNewItems * 7;
            for (var i = 1; i <= numberOfNewItems; i++)
            {
                list.Add(new NewsItem { Id = i + i, EventNumber = PooleParkRunId, PostDate = DateTimeOffset.Now.AddDays(-days) });
                days = days - 7;
            }

            _newsHandlerMock.Setup(x => x.GetNewsItems(PooleParkRunId, 10, It.IsAny<CancellationToken>()))
                .ReturnsAsync(list);
        }
    }
}
