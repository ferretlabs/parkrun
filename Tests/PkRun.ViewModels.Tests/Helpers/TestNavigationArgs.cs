﻿using Cimbalino.Toolkit.Services;

namespace PkRun.ViewModels.Test.Helpers
{
    public class TestNavArgs : NavigationServiceNavigationEventArgs
    {
        private TestNavArgs(NavigationServiceNavigationMode mode, object parameter)
            : base(mode, null, parameter, null)
        {
        }

        public static TestNavArgs Forward(object parameter)
        {
            return new TestNavArgs(NavigationServiceNavigationMode.Forward, parameter);
        }

        public static TestNavArgs Back(object parameter)
        {
            return new TestNavArgs(NavigationServiceNavigationMode.Back, parameter);
        }
    }
}
