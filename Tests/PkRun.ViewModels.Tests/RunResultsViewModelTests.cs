﻿using System;
using System.Threading.Tasks;
using Moq;
using ParkRunPortable;
using ParkRunPortable.Model;
using PkRun.Core.Services;
using PkRun.ViewModels.Test.Helpers;
using Xunit;

namespace PkRun.ViewModels.Test
{
    public class RunResultsViewModelTests : TestBaseViewModel
    {
        private readonly Mock<IParkRunClient> _parkRunMock = new Mock<IParkRunClient>();
        private readonly Mock<INavigationService> _navigationMock = new Mock<INavigationService>();
        private readonly Mock<IAuthenticationService> _authenticationMock = new Mock<IAuthenticationService>();

        private readonly RunResultsViewModel _subject;

        private readonly Run _pooleRunDetails = new Run
        {
            EventNumber = 233,
            EventDate = new DateTimeOffset(new DateTime(2017, 3, 4)),
            RunId = 308
        };

        public RunResultsViewModelTests()
        {
            _subject = new RunResultsViewModel(
                _parkRunMock.Object,
                _navigationMock.Object,
                _authenticationMock.Object);
        }

        [Fact]
        public async Task CollectionHasDetailsSetCorrectly()
        {
            await IfUserNavigatesToRunPage();

            Assert.Equal(233, _subject.Results.GetSource().EventId);
            Assert.Equal(308, _subject.Results.GetSource().RunId);
            Assert.Equal(_pooleRunDetails.EventDate, _subject.Results.GetSource().Date);
        }

        [Fact]
        public async Task PageTitleSetCorrectly()
        {
            await IfUserNavigatesToRunPage();
            
            Assert.Equal("Poole Park run - #308 - 04/03/2017", _subject.PageTitle);
        }

        private async Task IfUserNavigatesToRunPage()
        {
            var args = new RunResultsNavigationArgs(new Event
            {
                EventLongName = "Poole Park run",
                EventNumber = 233
            }, _pooleRunDetails);

            await _subject.OnNavigatedToAsync(TestNavArgs.Forward(args));

            _navigationMock.Verify(x => x.GoBack(), Times.Never);
        }
    }
}
