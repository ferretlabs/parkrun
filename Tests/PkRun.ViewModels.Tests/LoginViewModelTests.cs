﻿using System.Threading.Tasks;
using Moq;
using ParkRunPortable.Model;
using PkRun.Core.Services;
using Xunit;
using INavigationService = PkRun.Core.Services.INavigationService;

namespace PkRun.ViewModels.Test
{
    public class LoginViewModelTests : TestBaseViewModel
    {
        private readonly Mock<INavigationService> _navigationMock = new Mock<INavigationService>();
        private readonly Mock<IAuthenticationService> _authenticationMock = new Mock<IAuthenticationService>();
        private readonly LoginViewModel _subject;

        public LoginViewModelTests()
        {
            _subject = new LoginViewModel(
                _navigationMock.Object,
                _authenticationMock.Object);
        }

        [Fact]
        public void UsernameAndPasswordAreValid()
        {
            _subject.Username = "A31414";
            _subject.Password = "MyPassword";

            Assert.True(_subject.CanSignIn);
        }

        [Fact]
        public void UsernameValidAndPasswordInvalid()
        {
            _subject.Username = "A232323";
            _subject.Password = null;

            Assert.False(_subject.CanSignIn);
        }

        [Fact]
        public void UsernameInvalidAndPasswordValid()
        {
            _subject.Username = null;
            _subject.Password = "MyPassword";

            Assert.False(_subject.CanSignIn);
        }

        [Fact]
        public async Task GivenValidUsernameAndPasswordAreCorrectNavigateToHome()
        {
            _subject.Username = "A31414";
            _subject.Password = "MyPassword";

            await _subject.LoginCommand.ExecuteAsync(null);

            Assert.False(_subject.ShowErrorMessage);
        }

        [Fact]
        public async Task GivenValidUsernameAndPasswordAreIncorrectDontNavigateHome()
        {
            _authenticationMock.Setup(x => x.SignIn(It.IsAny<string>(), It.IsAny<string>()))
                               .Throws<ParkRunException>();

            _subject.Username = "A31414";
            _subject.Password = "MyPassword";

            await _subject.LoginCommand.ExecuteAsync(null);

            Assert.True(_subject.ShowErrorMessage);
        }
    }
}
