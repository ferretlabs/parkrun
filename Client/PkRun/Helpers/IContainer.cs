﻿namespace PkRun.Helpers
{
    public interface IContainer
    {
        void Create();
    }
}
