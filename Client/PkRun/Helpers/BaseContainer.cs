﻿using Cimbalino.Toolkit.Services;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using ModernHttpClient;
using ParkRunPortable;
using ParkRunPortable.Http;
using ParkRunPortable.Logging;
using PkRun.Core.Logging;
using PkRun.Core.Services;
using PkRun.Extensions;
using PkRun.Services;
using PkRun.Services.CimbalinoServices;
using INavigationService = PkRun.Core.Services.INavigationService;

namespace PkRun.Helpers
{
    public class BaseContainer : IContainer
    {
        public void Create()
        {
            if (!ServiceLocator.IsLocationProviderSet)
            {
                ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            }

            SetStorageService();
            SetLogger();
            SetAnalyticsService();
            SetParkRunClient();
            SetApplicationSettingsHandler();
            SetAuthentication();
            SetNavigation();
            SetNewsHandler();
            SetCancellationManager();
            SetDispatcher();
            SetEnvironmentService();
        }

        protected virtual void SetEnvironmentService()
        {
            If<IEnvironmentService, EnvironmentService>();
        }

        protected virtual void SetAnalyticsService()
        {
            //If<IAnalyticsService, HockeyAppAnalyticsService>();
            If<IAnalyticsService, NullAnalyticsService>();
        }

        protected virtual void SetLogger()
        {
            If<ILogger, PkRunLogger>();
        }

        protected virtual void SetParkRunClient()
        {
            var logger = SimpleIoc.Default.GetInstance<ILogger>();
            var client = new ParkRunClient(new PkRunHandlerFactory(), logger);
            SimpleIoc.Default.RegisterIf<IParkRunClient>(() => client);
        }

        protected virtual void SetAuthentication()
        {
            If<IAuthenticationService, AuthenticationService>();
        }

        protected virtual void SetNavigation()
        {
            If<INavigationService, ParkRunNavigationService>();
        }

        protected virtual void SetApplicationSettingsHandler()
        {
            If<IApplicationSettingsServiceHandler, ApplicationSettingsServiceHandler>();
        }

        protected virtual void SetStorageService()
        {
            If<IStorageService, StorageService>();
        }

        protected virtual void SetNewsHandler()
        {
            If<INewsApiHandler, NewsApiHandler>();
        }

        protected virtual void SetCancellationManager()
        {
            If<ICancellationManager, CancellationManager>();
        }

        protected virtual void SetDispatcher()
        {
            If<IDispatcherService, DispatcherService>();
        }

        protected void If<TInterface, TClass>()
            where TInterface : class
            where TClass : class, TInterface
        {
            SimpleIoc.Default.RegisterIf<TInterface, TClass>();
        }
    }

    public class PkRunHandlerFactory : IClientHandlerFactory
    {
        public IClientHandler CreateHandler()
        {
            return new PkRunHandler();
        }
    }

    public class PkRunHandler : NativeMessageHandler, IClientHandler
    {
        public bool IsDisposed { get; private set; }

        protected override void Dispose(bool disposing)
        {
            IsDisposed = true;
            base.Dispose(disposing);
        }
    }
}
