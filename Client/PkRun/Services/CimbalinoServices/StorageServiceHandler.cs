﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using PCLStorage;
using FileAccess = PCLStorage.FileAccess;

namespace PkRun.Services.CimbalinoServices
{
    public class StorageServiceHandler : IStorageServiceHandler
    {
        private readonly IFolder _rootFolder;

        public StorageServiceHandler(IFolder rootFolder)
        {
            _rootFolder = rootFolder;
        }

        public Uri BuildFileUri(string path)
        {
            var fullPath = Path.Combine(_rootFolder.Path, path);
            return new Uri(fullPath, UriKind.RelativeOrAbsolute);
        }

        public Task CopyFileAsync(string sourceFileName, string destinationFileName)
        {
            return CopyFileAsync(sourceFileName, destinationFileName, false);
        }

        public Task CopyFileAsync(string sourceFileName, string destinationFileName, bool overwrite)
        {
            throw new NotImplementedException();
        }

        public Task MoveFileAsync(string sourceFileName, string destinationFileName)
        {
            return MoveFileAsync(sourceFileName, destinationFileName, false);
        }

        public async Task MoveFileAsync(string sourceFileName, string destinationFileName, bool overwrite)
        {
            var sourceFile = await _rootFolder.GetFileAsync(sourceFileName);
            var option = overwrite ? NameCollisionOption.ReplaceExisting : NameCollisionOption.FailIfExists;

            await sourceFile.MoveAsync(destinationFileName, option);
        }

        public Task CreateDirectoryAsync(string dir)
        {
            return _rootFolder.CreateFolderAsync(dir, CreationCollisionOption.OpenIfExists);
        }

        public async Task<Stream> CreateFileAsync(string path)
        {
            return await CreateFileAsync(path, CreationCollisionOption.OpenIfExists);
        }

        public async Task DeleteDirectoryAsync(string dir)
        {
            var folder = await _rootFolder.GetFolderAsync(dir);
            await folder.DeleteAsync();
        }

        public async Task DeleteFileAsync(string path)
        {
            var file = await _rootFolder.GetFileAsync(path);
            await file.DeleteAsync();
        }

        public async Task<bool> DirectoryExistsAsync(string dir)
        {
            var result = await _rootFolder.CheckExistsAsync(dir);
            return result == ExistenceCheckResult.FolderExists;
        }

        public async Task<bool> FileExistsAsync(string path)
        {
            var result = await _rootFolder.CheckExistsAsync(path);
            return result == ExistenceCheckResult.FileExists;
        }

        public async Task<string[]> GetDirectoryNamesAsync()
        {
            var folders = await _rootFolder.GetFoldersAsync();
            return folders.Select(x => x.Name).ToArray();
        }

        public Task<string[]> GetDirectoryNamesAsync(string searchPattern)
        {
            throw new NotImplementedException();
        }

        public async Task<string[]> GetFileNamesAsync()
        {
            var files = await _rootFolder.GetFilesAsync();
            return files.Select(x => x.Name).ToArray();
        }

        public Task<string[]> GetFileNamesAsync(string searchPattern)
        {
            throw new NotImplementedException();
        }

        public async Task<Stream> OpenFileForReadAsync(string path)
        {
            var file = await _rootFolder.GetFileAsync(path);
            return await file.OpenAsync(FileAccess.Read);
        }

        public async Task<string> ReadAllTextAsync(string path)
        {
            var file = await _rootFolder.GetFileAsync(path);
            return await file.ReadAllTextAsync();
        }

        public Task<string> ReadAllTextAsync(string path, Encoding encoding)
        {
            throw new NotImplementedException();
        }

        public Task<string[]> ReadAllLinesAsync(string path)
        {
            throw new NotImplementedException();
        }

        public Task<string[]> ReadAllLinesAsync(string path, Encoding encoding)
        {
            throw new NotImplementedException();
        }

        public Task<byte[]> ReadAllBytesAsync(string path)
        {
            throw new NotImplementedException();
        }

        public async Task WriteAllTextAsync(string path, string contents)
        {
            var file = await _rootFolder.GetFileAsync(path);
            await file.WriteAllTextAsync(contents);
        }

        public Task WriteAllTextAsync(string path, string contents, Encoding encoding)
        {
            throw new NotImplementedException();
        }

        public Task WriteAllLinesAsync(string path, IEnumerable<string> contents)
        {
            throw new NotImplementedException();
        }

        public Task WriteAllLinesAsync(string path, IEnumerable<string> contents, Encoding encoding)
        {
            throw new NotImplementedException();
        }

        public Task WriteAllBytesAsync(string path, byte[] bytes)
        {
            throw new NotImplementedException();
        }

        public async Task AppendAllText(string path, string contents)
        {
            using (var file = await CreateFileAsync(path))
            {
                using (var streamWriter = new StreamWriter(file))
                {
                    await streamWriter.WriteAsync(contents).ConfigureAwait(false);
                }
            }
        }

        public async Task AppendAllText(string path, string contents, Encoding encoding)
        {
            using (var file = await CreateFileAsync(path))
            {
                using (var streamWriter = new StreamWriter(file, encoding))
                {
                    await streamWriter.WriteAsync(contents).ConfigureAwait(false);
                }
            }
        }

        public async Task AppendAllLines(string path, IEnumerable<string> contents)
        {
            using (var fileStream = await CreateFileAsync(path).ConfigureAwait(false))
            {
                using (var streamWriter = new StreamWriter(fileStream))
                {
                    foreach (var line in contents)
                    {
                        await streamWriter.WriteLineAsync(line).ConfigureAwait(false);
                    }
                }
            }
        }

        public async Task AppendAllLines(string path, IEnumerable<string> contents, Encoding encoding)
        {
            using (var fileStream = await CreateFileAsync(path).ConfigureAwait(false))
            {
                using (var streamWriter = new StreamWriter(fileStream, encoding))
                {
                    foreach (var line in contents)
                    {
                        await streamWriter.WriteLineAsync(line).ConfigureAwait(false);
                    }
                }
            }
        }

        private async Task<Stream> CreateFileAsync(string path, CreationCollisionOption options)
        {
            var file = await _rootFolder.CreateFileAsync(path, options);
            return await file.OpenAsync(FileAccess.ReadAndWrite);
        }
    }
}
