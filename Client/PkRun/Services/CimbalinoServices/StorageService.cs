﻿using Cimbalino.Toolkit.Services;
using PCLStorage;

namespace PkRun.Services.CimbalinoServices
{
    public class StorageService : IStorageService
    {
        public StorageService()
        {
            Local = new StorageServiceHandler(FileSystem.Current.LocalStorage);
            Roaming = new StorageServiceHandler(FileSystem.Current.RoamingStorage);
        }

        public IStorageServiceHandler Local { get; }
        public IStorageServiceHandler Roaming { get; }
        public IStorageServiceHandler Temporary { get; }
        public IStorageServiceHandler LocalCache { get; }
        public IStorageServiceHandler Package { get; }
    }
}
