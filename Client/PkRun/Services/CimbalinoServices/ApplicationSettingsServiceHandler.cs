using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace PkRun.Services.CimbalinoServices
{
    public class ApplicationSettingsServiceHandler : IApplicationSettingsServiceHandler
    {
        private static ISettings AppSettings => CrossSettings.Current;

        public bool Contains(string key)
        {
            return AppSettings.Contains(key);
        }

        public T Get<T>(string key)
        {
            return AppSettings.GetValueOrDefault<T>(key);
        }

        public T Get<T>(string key, T defaultValue)
        {
            return AppSettings.GetValueOrDefault(key, defaultValue);
        }

        public void Set<T>(string key, T value)
        {
            AppSettings.AddOrUpdateValue(key, value);
        }

        public void Remove(string key)
        {
            AppSettings.Remove(key);
        }

        public Task<IEnumerable<KeyValuePair<string, object>>> GetValuesAsync()
        {
            return Task.FromResult(Enumerable.Empty<KeyValuePair<string, object>>());
        }
    }
}