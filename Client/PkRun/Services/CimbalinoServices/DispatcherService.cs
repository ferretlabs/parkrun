﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using Xamarin.Forms;

namespace PkRun.Services.CimbalinoServices
{
    public class DispatcherService : IDispatcherService
    {
        public Task InvokeOnUiThreadAsync(Action action)
        {
            return InvokeOnUiThreadAsync(action, true);
        }

        public Task InvokeOnUiThreadAsync(Action action, bool force)
        {
            Device.BeginInvokeOnMainThread(action);
            return Task.FromResult(0);
        }

        public Task<T> InvokeOnUiThreadAsync<T>(Func<T> function)
        {
            throw new NotImplementedException();
        }

        public Task<T> InvokeOnUiThreadAsync<T>(Func<T> function, bool force)
        {
            throw new NotImplementedException();
        }

        public Task InvokeOnUiThreadAsync(Func<Task> asyncAction)
        {
            return InvokeOnUiThreadAsync(asyncAction, true);
        }

        public Task InvokeOnUiThreadAsync(Func<Task> asyncAction, bool force)
        {
            return Task.Run(asyncAction);
        }

        public Task<T> InvokeOnUiThreadAsync<T>(Func<Task<T>> asyncFunction)
        {
            throw new NotImplementedException();
        }

        public Task<T> InvokeOnUiThreadAsync<T>(Func<Task<T>> asyncFunction, bool force)
        {
            throw new NotImplementedException();
        }
    }
}
