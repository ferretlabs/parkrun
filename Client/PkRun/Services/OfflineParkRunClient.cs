﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ParkRunPortable;
using ParkRunPortable.Model;
using ParkRunPortable.Model.Requests;
using ParkRunPortable.Model.Responses;

namespace PkRun.Services
{
    public class OfflineParkRunClient : IParkRunClient
    {
        public Token Token { get; }
        public event EventHandler<Token> TokenUpdated;
        public void SetToken(Token token)
        {

        }

        public Task<Token> AuthenticateAsync(string username, string password, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public Task<Token> RefreshTokenAsync(string refreshToken, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public Task<Athlete> GetAthleteProfileAsync(int athleteId, bool expandedDetails = false, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public Task<Athlete> GetAthleteProfileAsync(string username, bool expandedDetails = false, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public Task<int> GetAthleteRunCountAsync(int athleteId, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public Task<ResultsResponse> GetRunsForAthleteAsync(int athleteId, FilterOptions filterOptions = null, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public Task<Event> GetEventAsync(int eventNumber, CancellationToken cancellationToken = new CancellationToken())
        {
            return Task.FromResult(new Event {EventLongName = "Poole Park Run", EventNumber = 233});
        }

        public Task<NewsResponse> GetNewsForEventAsync(int eventNumber, FilterOptions filterOptions = null, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public Task<NewsItem> GetNewsItemAsync(int eventNumber, int newsId, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public Task<CourseDataResponse> GetCourseDataAsync(int eventNumber, CancellationToken cancellationToken = new CancellationToken())
        {
            return Task.FromResult(new CourseDataResponse(new List<CourseDataInfo>(), 2));
        }

        public Task<string> GetCourseRouteMapAsync(int eventNumber, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public Task<RunsResponse> GetRunsForEventAsync(int eventNumber, FilterOptions filterOptions = null, CancellationToken cancellationToken = new CancellationToken())
        {
            var list = new List<Run>()
            {
                new Run() {EventDate = DateTimeOffset.Now, NumberRunners = 800, RunId = 1}
            };

            return Task.FromResult(new RunsResponse(list, 0));
        }

        private static int RandomNumber()
        {
            var generator = new Random();
            return generator.Next(0, 100000);
        }

        public Task<ResultsResponse> GetResultsForRunAsync(int eventNumber, int runId, DateTimeOffset dateOfRun, FilterOptions filterOptions = null, CancellationToken cancellationToken = new CancellationToken())
        {
            var offset = filterOptions?.Offset ?? 0;
            var list = new List<Result>();
            for (var i = offset; i < offset + 100; i++)
            {
                var result = new Result
                {
                    AthleteId = RandomNumber(),
                    FinishPosition = i,
                    FirstName = $"Bob{i}",
                    LastName = $"Smith{i}",
                    RunTime = TimeSpan.FromSeconds(900 + i)
                };

                if (i == 259)
                {
                    result.FirstName = "Mel";
                    result.LastName = "Lovegrove";
                    result.AthleteId = 2744074;
                }

                list.Add(result);
            }

            return Task.FromResult(new ResultsResponse(list, 800));
        }

        public Task<CountriesResponse> GetCountriesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public Task<ClubsResponse> GetClubsByCountryAsync(int countryCode, FilterOptions filterOptions = null, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public Task<AthletesResponse> SearchUsersAsync(string firstName, string lastName, int? countryCode = null, int? clubId = null, FilterOptions filterOptions = null, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public Task<int> SendFollowRequestAsync(int athleteId, CancellationToken cancellationToken = new CancellationToken())
        {
            throw new NotImplementedException();
        }
    }
}
