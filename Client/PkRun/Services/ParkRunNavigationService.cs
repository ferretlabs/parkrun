﻿using System.Threading.Tasks;
using PkRun.Core.Services;
using PkRun.Services.CimbalinoServices;
using PkRun.Views;
using INavigationService = PkRun.Core.Services.INavigationService;

namespace PkRun.Services
{
    public class ParkRunNavigationService : NavigationService, INavigationService
    {
        private readonly IAuthenticationService _authenticationService;

        public ParkRunNavigationService(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        public Task NavigateToHome(object parameters = null)
        {
            return _authenticationService.IsSignedIn ? NavigateToPage<MainView>(parameters) : NavigateToLogin(parameters);
        }

        public async Task NavigateToLogin(object parameters = null)
        {
            await NavigateToPage<LoginView>(parameters);
            ClearBackstack();
        }

        public Task NavigateToNews(object parameters = null)
        {
            throw new System.NotImplementedException();
        }

        public Task NavigateToNewsItem(object parameters = null)
        {
            throw new System.NotImplementedException();
        }

        public Task NavigateToAthlete(object parameters = null)
        {
            return NavigateToPage<AthleteView>(parameters);
        }

        public Task NavigateToSettings(object parameters = null)
        {
            return NavigateToPage<SettingsView>(parameters);
        }

        public Task NavigateToEvent(object parameters = null)
        {
            return NavigateToPage<EventView>(parameters);
        }

        public Task NavigateToRunResults(object parameters = null)
        {
            return NavigateToPage<RunResultsView>(parameters);
        }

        private Task NavigateToPage<T>(object parameters)
        {
            return Task.FromResult(Navigate<T>(parameters));
        }
    }
}