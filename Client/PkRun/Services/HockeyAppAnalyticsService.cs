﻿using HockeyApp;
using PkRun.Core.Services;

namespace PkRun.Services
{
    public class HockeyAppAnalyticsService : IAnalyticsService
    {
        public void Track(string message)
        {
            MetricsManager.TrackEvent(message);
        }
    }
}
