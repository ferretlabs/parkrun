﻿using Xamarin.Forms;

namespace PkRun.Controls
{
    public class NativeWebView : WebView
    {
        public static readonly BindableProperty MaxHeightProperty =
            BindableProperty.Create(nameof(MaxHeight), typeof(double), typeof(NativeWebView), default(double));

        public double MaxHeight
        {
            get { return (double) base.GetValue(MaxHeightProperty); }
            set { base.SetValue(MaxHeightProperty, value); }
        }

        public static readonly BindableProperty MaxWidthProperty =
            BindableProperty.Create(nameof(MaxWidth), typeof(double), typeof(NativeWebView), default(double));

        public double MaxWidth
        {
            get { return (double) base.GetValue(MaxWidthProperty); }
            set { base.SetValue(MaxWidthProperty, value); }
        }

        public static readonly BindableProperty HtmlProperty =
            BindableProperty.Create(nameof(Html), typeof(string), typeof(NativeWebView), default(string));

        public string Html
        {
            get { return (string) base.GetValue(HtmlProperty); }
            set { base.SetValue(HtmlProperty, value); }
        }
    }
}