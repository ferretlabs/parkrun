﻿using Xamarin.Forms;

namespace PkRun.Controls
{
    public class PkRunImage : Image
    {
        public static readonly BindableProperty UrlSourceProperty =
            BindableProperty.Create(nameof(UrlSource), typeof(string), typeof(PkRunImage), default(string), propertyChanged: OnUrlSourceChanged);

        public string UrlSource
        {
            get { return (string) GetValue(UrlSourceProperty); }
            set { SetValue(UrlSourceProperty, value); }
        }

        private static void OnUrlSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            (bindable as PkRunImage)?.SetImage();
        }

        private void SetImage()
        {
            Source = string.IsNullOrEmpty(UrlSource) ? ImageSource.FromResource("PkRun.Assets.DefaultProfile.png") : UrlSource;
        }
    }
}
