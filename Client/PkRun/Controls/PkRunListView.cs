﻿using Xamarin.Forms;

namespace PkRun.Controls
{
    public class PkRunListView : ListView
    {
        #region Ctors
        public PkRunListView()
        {
        }

        public PkRunListView(ListViewCachingStrategy cachingStrategy)
            : base(cachingStrategy)
        {
        }
        #endregion

        public static readonly BindableProperty IsVirtualisationOffProperty =
            BindableProperty.Create(nameof(IsVirtualisationOff), typeof(bool), typeof(PkRunListView), default(bool));

        public bool IsVirtualisationOff
        {
            get { return (bool) base.GetValue(IsVirtualisationOffProperty); }
            set { base.SetValue(IsVirtualisationOffProperty, value); }
        }
    }
}
