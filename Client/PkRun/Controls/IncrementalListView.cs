﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using PkRun.Core.Collections;
using Xamarin.Forms;

namespace PkRun.Controls
{
    /// <summary>
    /// IncrementalListView Interface
    /// </summary>
    public class IncrementalListView : PkRunListView
    {
        // Helper to keep track of what was last visible in the list
        private int _lastPosition;
        private IList _itemsSource;
        private ISupportIncrementalLoading _incrementalLoading;

        public IncrementalListView(ListViewCachingStrategy cachingStrategy)
            : base(cachingStrategy)
        {
            ItemAppearing += OnItemAppearing;
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == ItemsSourceProperty.PropertyName)
            {
                _itemsSource = ItemsSource as IList;

                if (_itemsSource == null)
                {
                    throw new Exception($"{nameof(IncrementalListView)} requires that {nameof(ItemsSource)} be of type IList");
                }

                _incrementalLoading = ItemsSource as ISupportIncrementalLoading;

                if (_incrementalLoading == null)
                {
                    System.Diagnostics.Debug.WriteLine($"{nameof(IncrementalListView)} BindingContext does not implement {nameof(ISupportIncrementalLoading)}. This is required for incremental loading to work.");
                }
            }
        }
        
        private void OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            int position = _itemsSource?.IndexOf(e.Item) ?? 0;

            if (_itemsSource != null)
            {
                // preloadIndex should never end up to be equal to itemsSource.Count otherwise
                // LoadMoreItems would not be called
                if (PreloadCount <= 0)
                    PreloadCount = 1;

                int preloadIndex = Math.Max(_itemsSource.Count - PreloadCount, 0);

                if ((position > _lastPosition || (position == _itemsSource.Count - 1)) && (position >= preloadIndex))
                {
                    _lastPosition = position;

                    if (!_incrementalLoading.IsLoadingIncrementally && !IsRefreshing && _incrementalLoading.HasMoreItems)
                    {
                        LoadMoreItems();
                    }
                }
            }
        }

        private void LoadMoreItems()
        {
            _incrementalLoading?.LoadMoreItemsAsync(1);
        }

        /// <summary>
        /// Identifies the <see cref="PreloadCount"/> bindable property.
        /// </summary>
        public static readonly BindableProperty PreloadCountProperty =
          BindableProperty.Create(nameof(PreloadCount), typeof(int), typeof(IncrementalListView), 0);

        /// <summary>
        /// How many cells before the end of the ListView before incremental loading should start. Defaults to 0, meaning the end of the list has to be reached before it will try to load more. This is a bindable property.
        /// </summary>
        public int PreloadCount
        {
            get { return (int)GetValue(PreloadCountProperty); }
            set { SetValue(PreloadCountProperty, value); }
        }
    }
}