﻿using GalaSoft.MvvmLight.Ioc;
using PkRun.Core.Services;
using PkRun.Helpers;
using PkRun.ViewModels;
using PkRun.Views;
using Xamarin.Forms;

namespace PkRun
{
    public partial class App
    {
        private static ViewModelLocator _locator;
        public static ViewModelLocator Locator => _locator ?? (_locator = new ViewModelLocator());

        private static INavigationService Navigation => SimpleIoc.Default.GetInstance<INavigationService>();
        private static IAuthenticationService Auth => SimpleIoc.Default.GetInstance<IAuthenticationService>();

        public App(IContainer container)
        {
            container.Create();
            
            InitializeComponent();
            
            SetNavigationHomePage();
        }

        private void SetNavigationHomePage()
        {
            Auth.AuthenticationChanged -= AuthOnAuthenticationChanged;
            Auth.AuthenticationChanged += AuthOnAuthenticationChanged;

            if (Auth.IsSignedIn)
            {
                var masterDetailPage = new MasterDetailPage
                {
                    MasterBehavior = MasterBehavior.Popover,
                    Master = new MasterPage { Title = "Menu" },
                    Detail = new ExtendedNavigationPage(Navigation, new MainView())
                };

                Navigation.NavigateToEvent(new EventNavigationArgs(Auth.SignedInAthlete.HomeRunId)).ContinueWith(_ => Navigation.ClearBackstack());

                MainPage = masterDetailPage;
            }
            else
            {
                MainPage = new ExtendedNavigationPage(Navigation, new LoginView());
            }
        }

        private void AuthOnAuthenticationChanged(object sender, AuthenticationChangedArgs args)
        {
            SetNavigationHomePage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
