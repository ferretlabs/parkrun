﻿using Xamarin.Forms.Xaml;

namespace PkRun.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPage
    {
        public MasterPage()
        {
            InitializeComponent();

            BindingContext = App.Locator.Nav;
        }
    }
}
