﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PkRun.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RunResultsView
    {
        public RunResultsView()
        {
            InitializeComponent();

            SetBinding(TitleProperty, new Binding(nameof(ViewModel.PageTitle)));
        }
    }
}
