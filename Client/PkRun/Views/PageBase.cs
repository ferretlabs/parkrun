﻿using PkRun.ViewModels;
using Xamarin.Forms;

namespace PkRun.Views
{
    public class PageBase : ContentPage
    {
        protected PageBaseViewModel ViewModel => BindingContext as PageBaseViewModel;

        public object NavigationParameter { get; private set; }

        public void SetNavigationParameter(object parameter)
        {
            NavigationParameter = parameter;
        }
    }
}
