﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace PkRun.ViewModels
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            if (!ServiceLocator.IsLocationProviderSet)
            {
                ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            }

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<LoginViewModel>();
            SimpleIoc.Default.Register<NavBarViewModel>();
            SimpleIoc.Default.Register<SettingsViewModel>();
            SimpleIoc.Default.Register<EventViewModel>();
            SimpleIoc.Default.Register<RunResultsViewModel>();
            SimpleIoc.Default.Register<AthleteViewModel>();
        }

        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();
        public LoginViewModel Login => ServiceLocator.Current.GetInstance<LoginViewModel>();
        public NavBarViewModel Nav => ServiceLocator.Current.GetInstance<NavBarViewModel>();
        public SettingsViewModel Settings => ServiceLocator.Current.GetInstance<SettingsViewModel>();
        public EventViewModel Event => ServiceLocator.Current.GetInstance<EventViewModel>();
        public RunResultsViewModel RunResults => ServiceLocator.Current.GetInstance<RunResultsViewModel>();
        public AthleteViewModel Athlete => ServiceLocator.Current.GetInstance<AthleteViewModel>();
    }
}
