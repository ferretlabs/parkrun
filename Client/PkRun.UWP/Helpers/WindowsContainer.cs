﻿using Cimbalino.Toolkit.Services;
using GalaSoft.MvvmLight.Ioc;
using ParkRunPortable;
using ParkRunPortable.Logging;
using PkRun.Core.Services;
using PkRun.Extensions;
using PkRun.Helpers;
using PkRun.UWP.Http;
using PkRun.UWP.Services;

namespace PkRun.UWP.Helpers
{
    public class WindowsContainer : BaseContainer
    {
        protected override void SetAnalyticsService()
        {
            //If<IAnalyticsService, WindowsHockeyAppAnalyticsService>();
            base.SetAnalyticsService();
        }

        protected override void SetParkRunClient()
        {
            var logger = SimpleIoc.Default.GetInstance<ILogger>();
            SimpleIoc.Default.RegisterIf<IParkRunClient>(() => new ParkRunClient(new WindowsClientHandlerFactory(), logger));
            //SimpleIoc.Default.RegisterIf<IParkRunClient>(() => new OfflineParkRunClient());
        }

        protected override void SetApplicationSettingsHandler()
        {
            SimpleIoc.Default.RegisterIf(() => new ApplicationSettingsService().Local);
        }

        protected override void SetStorageService()
        {
            If<IStorageService, StorageService>();
        }

        protected override void SetDispatcher()
        {
            If<IDispatcherService, DispatcherService>();
        }
    }
}
