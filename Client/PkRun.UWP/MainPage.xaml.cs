﻿using PkRun.UWP.Helpers;

namespace PkRun.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();

            LoadApplication(new PkRun.App(new WindowsContainer()));
        }
    }
}
