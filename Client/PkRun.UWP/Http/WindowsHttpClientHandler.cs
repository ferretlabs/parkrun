using System.Net.Http;
using ParkRunPortable.Http;

namespace PkRun.UWP.Http
{
    public class WindowsHttpClientHandler : HttpClientHandler, IClientHandler
    {
        public bool IsDisposed { get; private set; }

        protected override void Dispose(bool disposing)
        {
            IsDisposed = true;
            base.Dispose(disposing);
        }
    }
}