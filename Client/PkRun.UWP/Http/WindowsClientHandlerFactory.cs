﻿using System.Net;
using ParkRunPortable.Http;
using PkRun.UWP.Helpers;

namespace PkRun.UWP.Http
{
    public class WindowsClientHandlerFactory : IClientHandlerFactory
    {
        public IClientHandler CreateHandler()
        {
            return new WindowsHttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
            };
        }
    }
}