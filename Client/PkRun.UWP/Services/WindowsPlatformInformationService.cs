using System;
using Windows.Graphics.Display;
using Windows.System.Profile;
using Windows.UI.ViewManagement;
using PkRun.Core.Services;

namespace PkRun.UWP.Services
{
    public class WindowsPlatformInformationService : IPlatformInformationService
    {
        private static string _deviceFamily;

        public Platform Platform { get; } = GetDeviceInfo();

        private static Platform GetDeviceInfo()
        {
            var family = DeviceFamily();
            switch (family)
            {
                case "Windows.Mobile":
                    if (DisplayInformation.GetForCurrentView().DiagonalSizeInInches > 6
                        && UIViewSettings.GetForCurrentView().UserInteractionMode == UserInteractionMode.Mouse)
                    {
                        return Platform.WindowsDesktop;
                    }

                    return Platform.WindowsMobile;
                case "Windows.Desktop":
                    return Platform.WindowsDesktop;
                default:
                    throw new NotSupportedException();
            }
        }

        private static string DeviceFamily()
        {
            if (string.IsNullOrEmpty(_deviceFamily))
            {
                _deviceFamily = AnalyticsInfo.VersionInfo.DeviceFamily;
            }

            return _deviceFamily;
        }
    }
}