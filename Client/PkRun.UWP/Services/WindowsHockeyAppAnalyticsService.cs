﻿using Microsoft.HockeyApp;
using PkRun.Core.Services;

namespace PkRun.UWP.Services
{
    public class WindowsHockeyAppAnalyticsService : IAnalyticsService
    {
        public WindowsHockeyAppAnalyticsService()
        {
            HockeyClient.Current.Configure("f90d98d422784b33a88649e5529d722d");
        }

        public void Track(string message)
        {
            HockeyClient.Current.TrackEvent(message);
        }
    }
}
