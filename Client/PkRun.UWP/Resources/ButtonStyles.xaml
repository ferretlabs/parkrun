﻿<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml">

    <Style x:Key="EmptyButtonStyle"
           TargetType="ButtonBase">
        <Setter Property="Background"
                Value="Transparent" />
        <Setter Property="BorderThickness"
                Value="0" />
        <Setter Property="Padding"
                Value="0" />
        <Setter Property="HorizontalContentAlignment"
                Value="Stretch" />
        <Setter Property="HorizontalAlignment"
                Value="Stretch" />
        <Setter Property="MinWidth"
                Value="0" />
        <Setter Property="MinHeight"
                Value="0" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="ButtonBase">
                    <Border Background="{TemplateBinding Background}"
                            BorderThickness="{TemplateBinding BorderThickness}"
                            BorderBrush="{TemplateBinding BorderBrush}"
                            Padding="{TemplateBinding Padding}">
                        <VisualStateManager.VisualStateGroups>
                            <VisualStateGroup x:Name="CommonStates">
                                <VisualStateGroup.Transitions>
                                    <VisualTransition From="Pressed"
                                                      To="PointerOver">
                                        <Storyboard>
                                            <PointerUpThemeAnimation Storyboard.TargetName="Container" />
                                        </Storyboard>
                                    </VisualTransition>
                                    <VisualTransition From="PointerOver"
                                                      To="Normal">
                                        <Storyboard>
                                            <PointerUpThemeAnimation Storyboard.TargetName="Container" />
                                        </Storyboard>
                                    </VisualTransition>
                                    <VisualTransition From="Pressed"
                                                      To="Normal">
                                        <Storyboard>
                                            <PointerUpThemeAnimation Storyboard.TargetName="Container" />
                                        </Storyboard>
                                    </VisualTransition>
                                </VisualStateGroup.Transitions>
                                <VisualState x:Name="Normal" />
                                <VisualState x:Name="PointerOver" />
                                <VisualState x:Name="Pressed">
                                    <Storyboard>
                                        <PointerDownThemeAnimation Storyboard.TargetName="Container" />
                                    </Storyboard>
                                </VisualState>
                                <VisualState x:Name="Disabled">
                                    <Storyboard>
                                        <PointerUpThemeAnimation Storyboard.TargetName="Container" />
                                    </Storyboard>
                                </VisualState>
                            </VisualStateGroup>
                        </VisualStateManager.VisualStateGroups>
                        <ContentPresenter HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}"
                                          VerticalAlignment="{TemplateBinding VerticalContentAlignment}"
                                          Content="{TemplateBinding Content}"
                                          x:Name="Container"
                                          ContentTemplate="{TemplateBinding ContentTemplate}" />
                    </Border>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <Style x:Key="RoundCornerButtonStyle"
           TargetType="Button">
        <Setter Property="Background"
                Value="{ThemeResource ButtonBackground}" />
        <Setter Property="Foreground"
                Value="{ThemeResource ButtonForeground}" />
        <Setter Property="BorderBrush"
                Value="{ThemeResource ButtonBorderBrush}" />
        <Setter Property="BorderThickness"
                Value="2" />
        <Setter Property="Padding"
                Value="8" />
        <Setter Property="HorizontalAlignment"
                Value="Left" />
        <Setter Property="VerticalAlignment"
                Value="Center" />
        <Setter Property="FontFamily"
                Value="{ThemeResource ContentControlThemeFontFamily}" />
        <Setter Property="FontWeight"
                Value="Normal" />
        <Setter Property="FontSize"
                Value="{ThemeResource ControlContentThemeFontSize}" />
        <Setter Property="UseSystemFocusVisuals"
                Value="True" />
        <Setter Property="FocusVisualMargin"
                Value="-3" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="Button">
                    <Grid x:Name="RootGrid"
                          Background="{TemplateBinding Background}"
                          BorderBrush="{TemplateBinding BorderBrush}"
                          BorderThickness="{TemplateBinding BorderThickness}"
                          CornerRadius="5">
                        <VisualStateManager.VisualStateGroups>
                            <VisualStateGroup x:Name="CommonStates">
                                <VisualState x:Name="Normal">
                                    <Storyboard>
                                        <PointerUpThemeAnimation Storyboard.TargetName="RootGrid" />
                                    </Storyboard>
                                </VisualState>
                                <VisualState x:Name="PointerOver">
                                    <Storyboard>
                                        <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background"
                                                                       Storyboard.TargetName="RootGrid">
                                            <DiscreteObjectKeyFrame KeyTime="0"
                                                                    Value="{ThemeResource ButtonBackgroundPointerOver}" />
                                        </ObjectAnimationUsingKeyFrames>
                                        <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="BorderBrush"
                                                                       Storyboard.TargetName="RootGrid">
                                            <DiscreteObjectKeyFrame KeyTime="0"
                                                                    Value="{ThemeResource ButtonBorderBrushPointerOver}" />
                                        </ObjectAnimationUsingKeyFrames>
                                        <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground"
                                                                       Storyboard.TargetName="ContentPresenter">
                                            <DiscreteObjectKeyFrame KeyTime="0"
                                                                    Value="{ThemeResource ButtonForegroundPointerOver}" />
                                        </ObjectAnimationUsingKeyFrames>
                                        <PointerUpThemeAnimation Storyboard.TargetName="RootGrid" />
                                    </Storyboard>
                                </VisualState>
                                <VisualState x:Name="Pressed">
                                    <Storyboard>
                                        <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background"
                                                                       Storyboard.TargetName="RootGrid">
                                            <DiscreteObjectKeyFrame KeyTime="0"
                                                                    Value="{ThemeResource ButtonBackgroundPressed}" />
                                        </ObjectAnimationUsingKeyFrames>
                                        <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="BorderBrush"
                                                                       Storyboard.TargetName="RootGrid">
                                            <DiscreteObjectKeyFrame KeyTime="0"
                                                                    Value="{ThemeResource ButtonBorderBrushPressed}" />
                                        </ObjectAnimationUsingKeyFrames>
                                        <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground"
                                                                       Storyboard.TargetName="ContentPresenter">
                                            <DiscreteObjectKeyFrame KeyTime="0"
                                                                    Value="{ThemeResource ButtonForegroundPressed}" />
                                        </ObjectAnimationUsingKeyFrames>
                                        <PointerDownThemeAnimation Storyboard.TargetName="RootGrid" />
                                    </Storyboard>
                                </VisualState>
                                <VisualState x:Name="Disabled">
                                    <Storyboard>
                                        <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background"
                                                                       Storyboard.TargetName="RootGrid">
                                            <DiscreteObjectKeyFrame KeyTime="0"
                                                                    Value="{ThemeResource ButtonBackgroundDisabled}" />
                                        </ObjectAnimationUsingKeyFrames>
                                        <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="BorderBrush"
                                                                       Storyboard.TargetName="RootGrid">
                                            <DiscreteObjectKeyFrame KeyTime="0"
                                                                    Value="{ThemeResource ButtonBorderBrushDisabled}" />
                                        </ObjectAnimationUsingKeyFrames>
                                        <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground"
                                                                       Storyboard.TargetName="ContentPresenter">
                                            <DiscreteObjectKeyFrame KeyTime="0"
                                                                    Value="{ThemeResource ButtonForegroundDisabled}" />
                                        </ObjectAnimationUsingKeyFrames>
                                    </Storyboard>
                                </VisualState>
                            </VisualStateGroup>
                        </VisualStateManager.VisualStateGroups>
                        <ContentPresenter x:Name="ContentPresenter"
                                          AutomationProperties.AccessibilityView="Raw"
                                          ContentTemplate="{TemplateBinding ContentTemplate}"
                                          ContentTransitions="{TemplateBinding ContentTransitions}"
                                          Content="{TemplateBinding Content}"
                                          HorizontalContentAlignment="{TemplateBinding HorizontalContentAlignment}"
                                          Padding="{TemplateBinding Padding}"
                                          VerticalContentAlignment="{TemplateBinding VerticalContentAlignment}" />
                    </Grid>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>
    
    <Style TargetType="Button"
           BasedOn="{StaticResource RoundCornerButtonStyle}"/>
    
</ResourceDictionary>
