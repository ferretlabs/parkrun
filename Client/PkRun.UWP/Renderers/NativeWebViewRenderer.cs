﻿using System;
using System.ComponentModel;
using Windows.UI.Xaml.Controls;
using PkRun.Controls;
using PkRun.UWP.Renderers;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(NativeWebView), typeof(NativeWebViewRenderer))]
namespace PkRun.UWP.Renderers
{
    public class NativeWebViewRenderer : WebViewRenderer
    {
        private WebView _webView;
        private NativeWebView _nativeWebView;

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.WebView> e)
        {
            base.OnElementChanged(e);

            _webView = Control as WebView;

            _nativeWebView = e.NewElement as NativeWebView;

            if (_webView != null && !string.IsNullOrEmpty(_nativeWebView?.Html))
            {
                _webView.Navigate(new Uri(_nativeWebView.Html));
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == "Html")
            {
                if (_webView != null && !string.IsNullOrEmpty(_nativeWebView?.Html))
                {
                    _webView.Navigate(new Uri(_nativeWebView.Html));
                }
            }
            else if (e.PropertyName == "MaxHeight")
            {
                if (_webView != null && _nativeWebView != null)
                {
                    _webView.MaxHeight = _nativeWebView.MaxHeight;
                }
            }
            else if (e.PropertyName == "MaxWidth")
            {
                if (_webView != null && _nativeWebView != null)
                {
                    _webView.MaxWidth = _nativeWebView.MaxWidth;
                }
            }
        }
    }
}
