﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using PkRun.Controls;
using PkRun.UWP.Renderers;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(EmptyButton), typeof(EmptyButtonRenderer))]
namespace PkRun.UWP.Renderers
{
    public class EmptyButtonRenderer : ButtonRenderer
    {
        private Button _button;

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);

            _button = Control as Button;

            if (_button != null)
            {
                _button.Style = Application.Current.Resources["EmptyButtonStyle"] as Style;
            }
        }
    }
}
