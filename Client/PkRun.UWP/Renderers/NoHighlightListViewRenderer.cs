﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using PkRun.UWP.Renderers;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(Xamarin.Forms.ListView), typeof(NoHighlightListViewRenderer))]
namespace PkRun.UWP.Renderers
{
    public class NoHighlightListViewRenderer : ListViewRenderer
    {
        private ListView _listView;

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ListView> e)
        {
            base.OnElementChanged(e);

            _listView = Control as ListView;
            if (_listView != null)
            {
                if (e.NewElement?.StyleId == "NoHighlight")
                {
                    _listView.ItemContainerStyle = Application.Current.Resources["NoHighlight"] as Style;
                }
            }
        }
    }
}