﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using PkRun.Controls;
using PkRun.UWP.Renderers;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(PkRunListView), typeof(PkRunListViewRenderer))]
namespace PkRun.UWP.Renderers
{
    public class PkRunListViewRenderer : ListViewRenderer
    {
        private ListView _listView;

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ListView> e)
        {
            base.OnElementChanged(e);

            _listView = Control as ListView;
            
            var pkRunListView = e.NewElement as PkRunListView;
            if (pkRunListView != null && _listView != null)
            {
                if (pkRunListView.IsVirtualisationOff)
                {
                    _listView.ItemsPanel = Application.Current.Resources["NoVirtualisationPanel"] as ItemsPanelTemplate;
                }
            }
        }
    }
}
