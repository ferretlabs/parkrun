using HockeyApp.Android;
using PkRun.Helpers;
using Xamarin.Forms;

namespace PkRun.Droid.Helpers
{
    public class DroidContainer : BaseContainer
    {
        protected override void SetAnalyticsService()
        {
            CrashManager.Register(Forms.Context, "f0b427a6dcb5480e8cf21423015054a7");
            
            base.SetAnalyticsService();
        }
    }
}