﻿using Android.Content;
using Android.Views;
using PkRun.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using View = Android.Views.View;
using ListView = Android.Widget.ListView;

[assembly: ExportRenderer(typeof(TextCell), typeof(NoHighlightTextCellRenderer))]
namespace PkRun.Droid.Renderers
{
    public class NoHighlightTextCellRenderer : TextCellRenderer
    {
        protected override View GetCellCore(Cell item, View convertView, ViewGroup parent, Context context)
        {
            if (item.StyleId == "NoHighlight")
            {
                var listView = parent as ListView;
                if (listView != null)
                {
                    listView.SetSelector(Android.Resource.Color.Transparent);
                    listView.CacheColorHint = Color.Transparent.ToAndroid();
                }
            }

            return base.GetCellCore(item, convertView, parent, context);
        }
    }
}
