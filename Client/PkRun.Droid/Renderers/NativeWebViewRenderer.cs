using System.ComponentModel;
using PkRun.Controls;
using PkRun.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(NativeWebView), typeof(NativeWebViewRenderer))]
namespace PkRun.Droid.Renderers
{
    public class NativeWebViewRenderer : WebViewRenderer
    {
        private NativeWebView _nativeWebView;
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.WebView> e)
        {
            _nativeWebView = e.NewElement as NativeWebView;

            base.OnElementChanged(e);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == "Html" && _nativeWebView != null)
            {
                _nativeWebView.Source = _nativeWebView.Html;
            }
        }
    }
}