using System;
using PkRun.Core.Services;
using Xamarin.Forms;

namespace PkRun.Droid.Services
{
    public class DroidPlatformInformationService : IPlatformInformationService
    {
        public Platform Platform { get; } = GetDeviceInfo();

        private static Platform GetDeviceInfo()
        {
            var context = Forms.Context;
            // Compute screen size
            var dm = context.Resources.DisplayMetrics;
            var screenWidth = dm.WidthPixels / dm.Xdpi;
            var screenHeight = dm.HeightPixels / dm.Ydpi;
            var size = Math.Sqrt(Math.Pow(screenWidth, 2) + Math.Pow(screenHeight, 2));

            //
            // Tablet devices should have a screen size greater than 6 inches
            return size >= 6 ? Platform.AndroidTablet : Platform.AndroidMobile;
        }
    }
}