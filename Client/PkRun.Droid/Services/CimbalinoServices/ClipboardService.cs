using System;
using System.IO;
using System.Threading.Tasks;
using Android.Content;
using Cimbalino.Toolkit.Services;
using Xamarin.Forms;

namespace PkRun.Droid.Services.CimbalinoServices
{
    public class ClipboardService : IClipboardService
    {
        private static ClipboardManager Clipboard => (ClipboardManager) Forms.Context.GetSystemService(Context.ClipboardService);

        public Task<string> GetTextAsync()
        {
            return Task.FromResult(string.Empty);
        }

        public Task<Uri> GetWebLinkAsync()
        {
            throw new NotImplementedException();
        }

        public Task<string> GetHtmlAsync()
        {
            throw new NotImplementedException();
        }

        public Task<string> GetRtfAsync()
        {
            throw new NotImplementedException();
        }

        public Task<Uri> GetApplicationLinkAsync()
        {
            throw new NotImplementedException();
        }

        public Task<Stream> GetBitmapAsync()
        {
            throw new NotImplementedException();
        }

        public bool ContainsText { get; }
        public bool ContainsBitmap { get; }
        public bool ContainsHtml { get; }
        public bool ContainsRtf { get; }
        public bool ContainsWebLink { get; }
        public bool ContainsApplicationLink { get; }

        public void SetBitmap(Stream content)
        {
            throw new NotImplementedException();
        }

        public void SetHtml(string content)
        {
            var clip = ClipData.NewPlainText(string.Empty, content);
            Clipboard.PrimaryClip = clip;
        }

        public void SetRtf(string content)
        {
            var clip = ClipData.NewPlainText(string.Empty, content);
            Clipboard.PrimaryClip = clip;
        }

        public void SetApplicationLink(Uri content)
        {
            throw new NotImplementedException();
        }

        public void SetWebLink(Uri content)
        {
            throw new NotImplementedException();
        }

        public void SetText(string content)
        {
            var clip = ClipData.NewPlainText(string.Empty, content);
            Clipboard.PrimaryClip = clip;
        }
    }
}