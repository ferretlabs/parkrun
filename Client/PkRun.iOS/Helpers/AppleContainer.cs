﻿using HockeyApp.iOS;
using PkRun.Helpers;

namespace PkRun.iOS.Helpers
{
    public class AppleContainer : BaseContainer
    {
        protected override void SetAnalyticsService()
        {
            var manager = BITHockeyManager.SharedHockeyManager;
            manager.Configure("e3c6eed25a7246c099ca6225641c36e9");
            manager.StartManager();
            manager.Authenticator.AuthenticateInstallation();

            base.SetAnalyticsService();
        }
    }
}