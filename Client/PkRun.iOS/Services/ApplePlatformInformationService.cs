using System;
using PkRun.Core.Services;
using UIKit;

namespace PkRun.iOS.Services
{
    public class ApplePlatformInformationService : IPlatformInformationService
    {
        public Platform Platform { get; } = GetDeviceInfo();

        private static Platform GetDeviceInfo()
        {
            var type = UIDevice.CurrentDevice.UserInterfaceIdiom;
            switch (type)
            {
                case UIUserInterfaceIdiom.Pad:
                    return Platform.AppleTablet;
                case UIUserInterfaceIdiom.Phone:
                    return Platform.AppleMobile;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}