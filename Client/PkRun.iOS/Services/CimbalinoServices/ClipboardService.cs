using System;
using System.IO;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using UIKit;

namespace PkRun.iOS.Services.CimbalinoServices
{
    public class ClipboardService : IClipboardService
    {
        private static UIPasteboard Clipboard => UIPasteboard.General;

        public Task<string> GetTextAsync()
        {
            return Task.FromResult(string.Empty);
        }

        public Task<Uri> GetWebLinkAsync()
        {
            throw new NotImplementedException();
        }

        public Task<string> GetHtmlAsync()
        {
            throw new NotImplementedException();
        }

        public Task<string> GetRtfAsync()
        {
            throw new NotImplementedException();
        }

        public Task<Uri> GetApplicationLinkAsync()
        {
            throw new NotImplementedException();
        }

        public Task<Stream> GetBitmapAsync()
        {
            throw new NotImplementedException();
        }

        public bool ContainsText => Clipboard.HasStrings;
        public bool ContainsBitmap => Clipboard.HasUrls;
        public bool ContainsHtml => Clipboard.HasStrings;
        public bool ContainsRtf => Clipboard.HasStrings;
        public bool ContainsWebLink => Clipboard.HasUrls;
        public bool ContainsApplicationLink => Clipboard.HasUrls;

        public void SetBitmap(Stream content)
        {
            throw new NotImplementedException();
        }

        public void SetHtml(string content)
        {
            var clipboard = UIPasteboard.General;
            clipboard.String = content;
        }

        public void SetRtf(string content)
        {
            var clipboard = UIPasteboard.General;
            clipboard.String = content;
        }

        public void SetApplicationLink(Uri content)
        {
            throw new NotImplementedException();
        }

        public void SetWebLink(Uri content)
        {
            throw new NotImplementedException();
        }

        public void SetText(string content)
        {
            var clipboard = UIPasteboard.General;
            clipboard.String = content;
        }
    }
}