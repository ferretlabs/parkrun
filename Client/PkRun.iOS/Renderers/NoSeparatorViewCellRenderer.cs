﻿using PkRun.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ViewCell), typeof(NoSeparatorViewCellRenderer))]
namespace PkRun.iOS.Renderers
{
    public class NoSeparatorViewCellRenderer : ViewCellRenderer
    {
        public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
        {
            var cell = base.GetCell(item, reusableCell, tv);
            if (item.StyleId == "NoSeparator")
            {
                tv.SeparatorColor = UIColor.Clear;
                tv.SeparatorStyle = UITableViewCellSeparatorStyle.DoubleLineEtched;
            }

            return cell;
        }
    }
}