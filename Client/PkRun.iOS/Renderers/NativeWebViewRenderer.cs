using System.ComponentModel;
using PkRun.Controls;
using PkRun.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(NativeWebView), typeof(NativeWebViewRenderer))]
namespace PkRun.iOS.Renderers
{
    public class NativeWebViewRenderer : WebViewRenderer
    {
        private NativeWebView _nativeWebView;

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
            _nativeWebView = e.NewElement as NativeWebView;

            var oldWebView = e.OldElement as NativeWebView;
            if (oldWebView != null)
            {
                oldWebView.PropertyChanged -= NativeWebViewOnPropertyChanged;
            }

            if (_nativeWebView != null)
            {
                _nativeWebView.PropertyChanged += NativeWebViewOnPropertyChanged;
            }
        }

        private void NativeWebViewOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Html" && _nativeWebView != null)
            {
                _nativeWebView.Source = _nativeWebView.Html;
            }
        }
    }
}